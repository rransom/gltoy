
#include "shader.h"
#include "vecmat.h"
#include "scene.h"

#define ATTR_POSITION 0
#define ATTR_COLOR    1

static struct ShaderProgram *program_showaxes = NULL;

static const GLfloat axis_arrow_vertices[] = {
  -1.0, -0.1, 0.0, 1.0,
  -1.0, +0.1, 0.0, 1.0,
  -1.0,  0.0, 0.0, 1.0,
  +1.0,  0.0, 0.0, 1.0,
  +0.9, -0.1, 0.0, 1.0,
  +1.0,  0.0, 0.0, 1.0,
  +0.9, +0.1, 0.0, 1.0,
  +1.0,  0.0, 0.0, 1.0,
};

static const struct floatmat4 mat_identity = {{
    {1.0, 0.0, 0.0, 0.0},
    {0.0, 1.0, 0.0, 0.0},
    {0.0, 0.0, 1.0, 0.0},
    {0.0, 0.0, 0.0, 1.0},
  }};

static const struct floatmat4 mat_swap_xy = {{
    {0.0, 1.0, 0.0, 0.0},
    {1.0, 0.0, 0.0, 0.0},
    {0.0, 0.0, 1.0, 0.0},
    {0.0, 0.0, 0.0, 1.0},
  }};

static const f32v4 vec_color_white = { 1.0, 1.0, 1.0, 1.0 };
static const f32v4 vec_color_red = { 1.0, 0.0, 0.0, 1.0 };
static const f32v4 vec_color_green = { 0.0, 1.0, 0.0, 1.0 };

static void scene_init(struct scene_state *st) {
  program_showaxes = load_shader_program_by_name("showaxes");
};

static int scene_update(struct scene_state *st) {
  return 0;
};

static void scene_draw(struct scene_state *st) {
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT);

  use_program(program_showaxes);

  glEnableVertexAttribArray(ATTR_POSITION);
  glVertexAttribPointer(ATTR_POSITION, 4, GL_FLOAT, GL_FALSE, 0, axis_arrow_vertices);

  glDisableVertexAttribArray(ATTR_COLOR);

  program_set_uniform_mat4(program_showaxes, "transform", &mat_identity);
  glVertexAttrib4fv(ATTR_COLOR, (const GLfloat *)&vec_color_red);
  glDrawArrays(GL_LINES, 0, 8);

  program_set_uniform_mat4(program_showaxes, "transform", &mat_swap_xy);
  glVertexAttrib4fv(ATTR_COLOR, (const GLfloat *)&vec_color_green);
  glDrawArrays(GL_LINES, 0, 8);
};

static void scene_cleanup(struct scene_state *st) {
  /* TODO can't be implemented yet */
};

const struct scene_impl scene_showaxes = {
  scene_init,
  scene_update,
  scene_draw,
  scene_cleanup,
};

