
#include "texture_asset.h"
#include "asset.h"

void texture_part_asset_loader(const struct texture_part *part, struct texture *tex) {
  size_t size = 0;
  uint8_t *data = asset_load_blob(part->part_name, &size);

  set_texture_image_any(tex, part->target, data, size);

  SDL_free(data);
};

struct texture *load_texture_from_asset(const char *name, GLenum target) {
  struct texture_part part = {
    texture_part_asset_loader,
    name,
    target
  };
  struct texture_part *parts[] = { &part, NULL };
  struct texture_spec spec = { target, name, parts, NULL };
  return load_texture(&spec);
};

