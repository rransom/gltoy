
#include "model.h"

#include "shader.h"
#include "shader_asset.h"

#include <assert.h>
#include <string.h>

static int is_power_of_two(GLuint x) {
  GLuint is_npot = x & (x - 1);
  return !is_npot;
};

static GLsizeiptr align_for_next_chunk(GLsizeiptr fillptr, GLuint align) {
  if (align <= 1) return fillptr;

  assert(is_power_of_two(align));

  {
    GLuint alignmask = align - 1;
    while (fillptr & alignmask) ++fillptr;
  };

  return fillptr;
};

struct buffer_target_type {
  GLenum target;
  GLenum binding_query;
};
static const struct buffer_target_type buffer_target_types[] = {
  { GL_ARRAY_BUFFER, GL_ARRAY_BUFFER_BINDING },
  { GL_ELEMENT_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER_BINDING },
};
static const size_t num_buffer_target_types = sizeof(buffer_target_types)/sizeof(buffer_target_types[0]);
#define STATIC_MAX_NUM_BUFFER_TARGET_TYPES 4

struct buffer_fill_plan_tmp {
  GLuint origbuf;
  GLuint buf;
  GLsizeiptr len;
  GLsizeiptr fillptr;
};

void model_buffer_chunk_prepare(struct model_buffer_chunk **chunks) {
  struct buffer_fill_plan_tmp fillplans[STATIC_MAX_NUM_BUFFER_TARGET_TYPES];
  size_t curtype;
  struct model_buffer_chunk **curchunk;

  assert(STATIC_MAX_NUM_BUFFER_TARGET_TYPES >= num_buffer_target_types);

  memset(fillplans, 0, sizeof(fillplans));

  for (curchunk = chunks; *curchunk != NULL; ++curchunk) {
    struct model_buffer_chunk * const ch = *curchunk;
    for (curtype = 0; curtype < num_buffer_target_types; ++curtype) {
      if (buffer_target_types[curtype].target == ch->buffer_target) {
        fillplans[curtype].len = align_for_next_chunk(fillplans[curtype].len, ch->align);
        fillplans[curtype].len += ch->size;
        break;
      };
    };
  };

  for (curtype = 0; curtype < num_buffer_target_types; ++curtype) {
    if (fillplans[curtype].len != 0) {
      const struct buffer_target_type *bttype = &(buffer_target_types[curtype]);
      struct buffer_fill_plan_tmp * const plan = &(fillplans[curtype]);
      glGenBuffers(1, &(plan->buf));
      glGetIntegerv(bttype->binding_query, &(plan->origbuf));
      glBindBuffer(bttype->target, plan->buf);
      glBufferData(bttype->target, plan->len, NULL, GL_STATIC_DRAW);

      for (curchunk = chunks; *curchunk != NULL; ++curchunk) {
        struct model_buffer_chunk * const ch = *curchunk;
        if (ch->buffer_target != bttype->target) continue;

        glBufferSubData(bttype->target, plan->fillptr, ch->size, ch->client_pointer);

        ch->is_in_buffer = 1;
        ch->buffer = plan->buf;
        ch->buffer_pointer = (const void *)(plan->fillptr);

        plan->fillptr = align_for_next_chunk(plan->fillptr, ch->align);
        plan->fillptr += ch->size;
      };

      assert(plan->fillptr == plan->len);
    };
  };

  for (curtype = 0; curtype < num_buffer_target_types; ++curtype) {
    if (fillplans[curtype].len != 0) {
      const struct buffer_target_type *bttype = &(buffer_target_types[curtype]);
      struct buffer_fill_plan_tmp * const plan = &(fillplans[curtype]);
      glBindBuffer(bttype->target, plan->origbuf);
    };
  };
};

void model_drawpart_prepare(struct model_drawpart *dp) {
  link_program(dp->program);
  program_prepare_uniform_bindings(dp->program, dp->prog_uniforms);
};

void model_prepare(struct model *model) {
  model_buffer_chunk_prepare(model->buffer_chunks);

  assert(model->drawparts != NULL);
  {
    struct model_drawpart **dps = model->drawparts;
    while (*dps != NULL) {
      model_drawpart_prepare(*dps);
      ++dps;
    };
  };

  assert(model->is_in_vao == 0);
  assert(model->vao == 0);
};

void model_prepare_parameters(struct model *model, struct program_uniform_binding **uniforms) {
  /* FIXME the current design can't support this */               
};

void model_drawpart_draw(struct model_drawpart *dp, struct program_uniform_binding **uniforms) {
  use_program(dp->program);

  program_prepare_uniform_bindings(dp->program, dp->prog_uniforms);
  program_set_uniform_bindings(dp->program, dp->prog_uniforms);

  program_prepare_uniform_bindings(dp->program, uniforms);
  program_set_uniform_bindings(dp->program, uniforms);

  texture_bindings_make_active(dp->textures);

  {
    struct model_drawpart_element_array *ea = dp->element_array;
    glBindBuffer(ea->data->buffer_target, ea->data->buffer);
    glDrawElements(ea->mode, ea->count, ea->type, ea->data->buffer_pointer);
  };
};

void model_draw(struct model *model, struct program_uniform_binding **uniforms) {
  if (model->attrib_consts != NULL) {
    struct model_attrib_const **acs;
    for (acs = model->attrib_consts; *acs != NULL; ++acs) {
      struct model_attrib_const *ac = *acs;
      glDisableVertexAttribArray(ac->index);
      glVertexAttrib4fv(ac->index, ac->value);
    };
  };

  assert(model->is_in_vao == 0);
  assert(model->vao == 0);
  /* FIXME use vertex array objects when available */               

  if (!(model->is_in_vao) && model->attrib_arrays != NULL) {
    struct model_attrib_array **aas;
    for (aas = model->attrib_arrays; *aas != NULL; ++aas) {
      struct model_attrib_array *aa = *aas;
      glEnableVertexAttribArray(aa->index);
      glBindBuffer(aa->data->buffer_target, aa->data->buffer);
      glVertexAttribPointer(aa->index, aa->attrib_value_size, aa->type, aa->normalized, aa->stride, aa->data->buffer_pointer);
    };
  };

  {
    struct model_drawpart **dps = model->drawparts;
    for (dps = model->drawparts; *dps != NULL; ++dps) {
      model_drawpart_draw(*dps, uniforms);
    };
  };
};

static const struct attribute_binding attbind_vPosition  = {"vPosition", MODEL_STDATTR_POSITION };
static const struct attribute_binding attbind_vTexCoord  = {"vTexCoord", MODEL_STDATTR_TEXCOORD };
static const struct attribute_binding attbind_vNormal    = {"vNormal",   MODEL_STDATTR_NORMAL   };
static const struct attribute_binding attbind_vTangentS  = {"vTangentS", MODEL_STDATTR_TANGENT_S};
static const struct attribute_binding attbind_vTangentT  = {"vTangentT", MODEL_STDATTR_TANGENT_T};
const struct attribute_binding *model_stdattbinds[] = {
  &attbind_vPosition,
  &attbind_vTexCoord,
  &attbind_vNormal,
  &attbind_vTangentS,
  &attbind_vTangentT,
  NULL
};

static struct shader_part model_shader_stdmodel_vertex_part = {
  shader_part_asset_loader,
  "shaders/stdmodel-vertex.glsl",
  NULL
};
static struct shader_part *model_shader_stdmodel_vertex_parts[] = {
  &model_shader_stdmodel_vertex_part,
  NULL
};
static struct shader_spec model_shader_stdmodel_vertex = {
  GL_VERTEX_SHADER,
  "stdmodel-vertex",
  model_shader_stdmodel_vertex_parts,
  0
};

static struct shader_part model_shader_stdmodel_fragment_part = {
  shader_part_asset_loader,
  "shaders/stdmodel-fragment.glsl",
  NULL
};
static struct shader_part *model_shader_stdmodel_fragment_parts[] = {
  &model_shader_stdmodel_fragment_part,
  NULL
};
static struct shader_spec model_shader_stdmodel_fragment = {
  GL_FRAGMENT_SHADER,
  "stdmodel-fragment",
  model_shader_stdmodel_fragment_parts,
  0
};

static struct shader_spec *model_shaders_stdmodel[] = {
  &model_shader_stdmodel_vertex,
  &model_shader_stdmodel_fragment,
  NULL
};

struct program_spec model_shader_program_stdmodel = {
  "stdmodel",
  model_shaders_stdmodel,
  model_stdattbinds,
  0
};

