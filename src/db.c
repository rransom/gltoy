
#include "db.h"
#include "util.h"

#include <stdio.h>

#include <sys/sbuf.h>

static int initialized = 0;
static const char *orgname = NULL;
static const char *appname = NULL;

static char *pref_path = NULL;

static sqlite3 *dbconn = NULL;

static struct db_static_query dbsq_head = { &dbsq_head, NULL, "! list-head sentinel", NULL };

static void errorLogCallback(void *pArg, int iErrCode, const char *zMsg) {
  SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "SQLite log message (%d): %s", iErrCode, zMsg);
};

static void report_sqlite_error_nodb(const char *ctx, int errcode) {
  SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SQLite error %s (%d)", ctx, errcode);
};

void db_vreportf_error(int errcode, const char *ctx, va_list ap) {
  struct sbuf *sb = sbuf_new_auto();
  const char *sqlite_msg = (dbconn == NULL) ? "(dbconn is NULL)" : sqlite3_errmsg(dbconn);
  const char *ctx_formatted = ctx;

  sbuf_vprintf(sb, ctx, ap);

  if (sbuf_finish(sb) == 0) ctx_formatted = sbuf_data(sb);

  SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SQLite error %s (%d): %s",
               ctx_formatted, errcode, sqlite_msg);

  sbuf_delete(sb);
};
void db_reportf_error(int errcode, const char *ctx, ...) {
  va_list ap;

  va_start(ap, ctx);
  db_vreportf_error(errcode, ctx, ap);
  va_end(ap);
};
void db_report_error(const char *ctx, int errcode) {
  db_reportf_error(errcode, "%s", ctx);
};

#if defined(__WINDOWS__) || defined(__WINRT__)
static int my_isalpha(char ch) {
  return (((ch >= 'A') && (ch <= 'Z')) ||
          ((ch >= 'a') && (ch <= 'z')));
};
#endif

static int is_pathsep(char ch) {
  return ((ch == '/')
#if defined(__WINDOWS__) || defined(__WINRT__)
          || (ch == '\\')
#endif
          );
};

static void func_urlify_path(sqlite3_context *ctx, int argc, sqlite3_value **argv) {
  struct sbuf acc_;
  struct sbuf *acc;
  const char *path;
  size_t pathlen;
  size_t i;

  if (argc != 1) {
    sqlite3_result_error(ctx, "OOPS zfkvjn-rfi-xmqmk", -1);
    sqlite3_result_error_code(ctx, SQLITE_MISUSE);
    return;
  };

  path = sqlite3_value_text(argv[0]);
  pathlen = sqlite3_value_bytes(argv[0]);

  acc = sbuf_new(&acc_, NULL, pathlen + 16, SBUF_AUTOEXTEND);

  /* 6. Prepend the "file:" scheme. */
  sbuf_cat(acc, "file:");

#if defined(__WINDOWS__) || defined(__WINRT__)
  /* 5. On windows only, if the filename begins with a drive letter, prepend a single "/" character. */
  if ((pathlen >= 2) && my_isalpha(path[0]) && (path[1] == ':')) {
    sbuf_cat(acc, "/");
  };
#endif

  i = 0;
  while (i < pathlen) {
    if (is_pathsep(path[i])) {
      /* 3. On windows only, convert all "\" characters into "/". */
      sbuf_cat(acc, "/"); ++i;

      /* 4. Convert all sequences of two or more "/" characters into a single "/" character. */
      while ((i < pathlen) && is_pathsep(path[i])) ++i;
    } else if (path[i] == '?') {
      /* 1. Convert all "?" characters into "%3f". */
      sbuf_cat(acc, "%3f"); ++i;
    } else if (path[i] == '#') {
      /* 2. Convert all "#" characters into "%23". */
      sbuf_cat(acc, "%23"); ++i;
    } else {
      sbuf_putc(acc, path[i]); ++i;
    };
  };

  if (sbuf_finish(acc) < 0) goto oom;

  sqlite3_result_text(ctx, sbuf_data(acc), sbuf_len(acc), SQLITE_TRANSIENT);
  sbuf_delete(acc);
  return;

 oom:
  sqlite3_result_error_nomem(ctx);
  return;
};

static int db_init_functions() {
  int frv = -1;
  int rv;

  rv = sqlite3_create_function(dbconn, "urlify_path", 1, SQLITE_UTF8, NULL, func_urlify_path, NULL, NULL);
  if (rv != SQLITE_OK) {
    db_report_error("db_init_functions create_function urlify_path", rv);
    goto end;
  };

  frv = 0;

 end:
  return frv;
};

static int db_exec_zzz(const char *sql, const char *zArg1, const char *zArg2, const char *zArg3) {
  int frv = -1;
  int rv;
  int nRows;
  sqlite3_stmt *stmt = NULL;

  rv = sqlite3_prepare_v2(dbconn, sql, -1, &stmt, NULL);
  if (rv != SQLITE_OK) {
    db_report_error("db_exec_zzz prepare", rv);
    goto end;
  };

  rv = sqlite3_bind_text(stmt, 1, zArg1, -1, SQLITE_STATIC);
  if (rv != SQLITE_OK) {
    db_report_error("db_exec_zzz bind arg 1", rv);
    goto end;
  };

  rv = sqlite3_bind_text(stmt, 2, zArg2, -1, SQLITE_STATIC);
  if (rv != SQLITE_OK) {
    db_report_error("db_exec_zzz bind arg 2", rv);
    goto end;
  };

  rv = sqlite3_bind_text(stmt, 3, zArg3, -1, SQLITE_STATIC);
  if (rv != SQLITE_OK) {
    db_report_error("db_exec_zzz bind arg 3", rv);
    goto end;
  };

  nRows = 0;
  rv = sqlite3_step(stmt);

  while (rv == SQLITE_ROW) {
    ++nRows;
    rv = sqlite3_step(stmt);
  };

  if (nRows != 0) SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "db_exec_zzz: query returned %d rows (%s)", nRows, sql);

  if (rv != SQLITE_DONE) {
    db_report_error("db_exec_zzz step", rv);
    goto end;
  };

  frv = 0;

 end:
  rv = sqlite3_finalize(stmt); stmt = NULL;
  if (rv != SQLITE_OK) {
    db_report_error("db_exec_zzz finalize", rv);
  };

  return frv;
};

static int db_attach_readonly(const char *schemaname, const char *filename) {
  int frv = -1;
  char *base_path = SDL_GetBasePath();

  if (db_exec_zzz("ATTACH DATABASE urlify_path(?3) || 'dat/' || ?2 || '?mode=ro' AS ?1;",
                  schemaname, filename, base_path) == 0) goto success;

  if (db_exec_zzz("ATTACH DATABASE urlify_path(?3) || '../dat/' || ?2 || '?mode=ro' AS ?1;",
                  schemaname, filename, base_path) == 0) goto success;

  /* FIXME? try ~/.local/share/ or XDG equivalent? */               
  
  

 end:
  if (base_path != NULL) {
    SDL_free(base_path);
    base_path = NULL;
  };

  return frv;

 success:
  frv = 0;
  goto end;
};

int db_init(const char *orgname_, const char *appname_) {
  int rv;

  if (initialized) return 0;
  if (orgname_ == NULL) orgname_ = "No Name";
  if (appname_ == NULL) return 0;

  if (util_test_is_rel_path_safe() != 0) {
    return -1;
  };

  initialized = 1;
  orgname = orgname_;
  appname = appname_;

  pref_path = SDL_GetPrefPath(orgname, appname);

  rv = sqlite3_config(SQLITE_CONFIG_LOG, errorLogCallback, NULL);
  if (rv != SQLITE_OK) report_sqlite_error_nodb("setting SQLITE_CONFIG_LOG", rv);

  rv = sqlite3_initialize();
  if (rv != SQLITE_OK) report_sqlite_error_nodb("in sqlite3_initialize", rv);

  {
    char *globaldb_filename = util_strdupcat(pref_path, "globalstate.sqlite");

    rv = sqlite3_open_v2(globaldb_filename, &dbconn, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_URI, NULL);

    SDL_free(globaldb_filename);

    if ((rv != SQLITE_OK) || (dbconn == NULL)) {
      db_report_error("in sqlite3_open_v2", rv);
      return -1;
    };
  };

  if (db_init_functions() < 0) return -1;

  rv = sqlite3_exec(dbconn, "PRAGMA foreign_keys = ON;", NULL, NULL, NULL);
  if (rv != SQLITE_OK) {
    db_report_error("in sqlite3_exec PRAGMA foreign_keys = ON", rv);
    return -1;
  };

  rv = sqlite3_exec(dbconn, "PRAGMA recursive_triggers = ON;", NULL, NULL, NULL);
  if (rv != SQLITE_OK) {
    db_report_error("in sqlite3_exec PRAGMA recursive_triggers = ON", rv);
    return -1;
  };

  /* FIXME on Android, set "PRAGMA locking_mode = EXCLUSIVE;" */               
  
  
  

  /* FIXME use read-only database for assets for now */               
  if (db_attach_readonly("assets", "assets-ro.sqlite") < 0) return -1;

  
  
  

  
  
  
};

void db_quit() {
  struct db_static_query *dbsq;

  if (initialized) {
    for (dbsq = dbsq_head.next; dbsq != &dbsq_head; dbsq = dbsq->next) {
      if (dbsq->stmt != NULL) {
        sqlite3_finalize(dbsq->stmt);
        dbsq->stmt = NULL;
      };
    };

    if (dbconn != NULL) {
      sqlite3_close_v2(dbconn);
      dbconn = NULL;
    };

    if (pref_path != NULL) {
      SDL_free(pref_path);
      pref_path = NULL;
    };

    initialized = 0;
  };
};

static void dbsq_log_misuse(const char *funcname, struct db_static_query *dbsq, const char *misuse_manner) {
  const char *dbsq_name;
  if (dbsq == NULL) {
    dbsq_name = "(dbsq is NULL)";
  } else if (dbsq->name == NULL) {
    dbsq_name = "(dbsq->name is NULL)";
  } else {
    dbsq_name = dbsq->name;
  };
  SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s %s: %s", funcname, dbsq_name, misuse_manner);
};

int dbsq_start(struct db_static_query *dbsq) {
  int rv;

  if (dbsq == NULL) {
    dbsq_log_misuse("dbsq_start", dbsq, "NULL query");
    return -1;
  };

  if (dbsq->next == NULL) {
    dbsq->next = dbsq_head.next;
    dbsq_head.next = dbsq->next;
  };

  if (dbsq->stmt == NULL) {
    const char *tail = NULL;
    rv = sqlite3_prepare_v3(dbconn, dbsq->sql, -1, SQLITE_PREPARE_PERSISTENT, &(dbsq->stmt), &tail);
    if (rv != SQLITE_OK) {
      db_reportf_error(rv, "dbsq_start %s", dbsq->name);
      return -1;
    };
  };

  rv = sqlite3_reset(dbsq->stmt);
  /* ignore rv; it was logged already by the step call */

  dbsq->state = DBSQ_STATE_STARTED;
  dbsq->query_failed = 0;

  return 0;
};

void dbsq_bind_null(struct db_static_query *dbsq, int paramindex) {
  int rv;

  if (dbsq == NULL) {
    dbsq_log_misuse("dbsq_bind_null", dbsq, "NULL query");
    return;
  };

  if (dbsq->state != DBSQ_STATE_STARTED) {
    dbsq_log_misuse("dbsq_bind_null", dbsq, "incorrect state");
    return;
  };

  rv = sqlite3_bind_null(dbsq->stmt, paramindex);
  if (rv != SQLITE_OK) {
    db_reportf_error(rv, "dbsq_bind_null %s %d", dbsq->name, paramindex);
    dbsq->query_failed = 1;
  };
};

void dbsq_bind_text(struct db_static_query *dbsq, int paramindex, const char *value) {
  int rv;

  if (dbsq == NULL) {
    dbsq_log_misuse("dbsq_bind_text", dbsq, "NULL query");
    return;
  };

  if (dbsq->state != DBSQ_STATE_STARTED) {
    dbsq_log_misuse("dbsq_bind_text", dbsq, "incorrect state");
    return;
  };

  rv = sqlite3_bind_text(dbsq->stmt, paramindex, value, -1, SQLITE_TRANSIENT);
  if (rv != SQLITE_OK) {
    db_reportf_error(rv, "dbsq_bind_text %s %d", dbsq->name, paramindex);
    dbsq->query_failed = 1;
  };
};

void dbsq_bind_int64(struct db_static_query *dbsq, int paramindex, int64_t value) {
  int rv;

  if (dbsq == NULL) {
    dbsq_log_misuse("dbsq_bind_int64", dbsq, "NULL query");
    return;
  };

  if (dbsq->state != DBSQ_STATE_STARTED) {
    dbsq_log_misuse("dbsq_bind_int64", dbsq, "incorrect state");
    return;
  };

  rv = sqlite3_bind_int64(dbsq->stmt, paramindex, value);
  if (rv != SQLITE_OK) {
    db_reportf_error(rv, "dbsq_bind_int64 %s %d", dbsq->name, paramindex);
    dbsq->query_failed = 1;
  };
};

int dbsq_step(struct db_static_query *dbsq) {
  int rv;

  if (dbsq == NULL) {
    dbsq_log_misuse("dbsq_done", dbsq, "NULL query");
    return 0;
  };

  if ((dbsq->state != DBSQ_STATE_STARTED) && (dbsq->state != DBSQ_STATE_STEPPED)) {
    dbsq_log_misuse("dbsq_step", dbsq, "incorrect state");
    return 0;
  };

  if (dbsq->query_failed) {
    dbsq_log_misuse("dbsq_step", dbsq, "called on failed query");
    return 0;
  };

  rv = sqlite3_step(dbsq->stmt);

  dbsq->state = DBSQ_STATE_STEPPED;

  if (rv == SQLITE_ROW) {
    return 1;
  } else if (rv == SQLITE_DONE) {
    return 0;
  } else {
    db_reportf_error(rv, "dbsq_step %s", dbsq->name);
    dbsq->query_failed = 1;
    return 0;
  };
};

void dbsq_done(struct db_static_query *dbsq) {
  if (dbsq == NULL) {
    dbsq_log_misuse("dbsq_done", dbsq, "NULL query");
    return;
  };

  if (dbsq->state == DBSQ_STATE_UNINITIALIZED) {
    dbsq_log_misuse("dbsq_done", dbsq, "incorrect state (uninitialized)");
    return;
  };

  if (dbsq->state == DBSQ_STATE_INACTIVE) {
    dbsq_log_misuse("dbsq_done", dbsq, "incorrect state");
  };

  dbsq->state = DBSQ_STATE_INACTIVE;
  dbsq->query_failed = 0;

  if (dbsq->stmt != NULL) {
    sqlite3_reset(dbsq->stmt);
  };
};

