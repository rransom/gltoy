
#include <math.h>

#include <SDL.h>
#include <SDL_opengles2.h>

#include "db.h"
#include "asset.h"
#include "util.h"

#include "texture.h"
#include "scene.h"

static SDL_Window *win = NULL;
static SDL_GLContext glctx = NULL;

static SDL_EventType USEREVENT_DRAW_SCENE;

extern const struct scene_impl scene_showaxes;
extern const struct scene_impl scene_testblit1;

static const struct scene_impl *scene = &scene_testblit1;
static struct scene_state scene_state;
static SDL_TimerID scene_update_timer = 0;

static void queue_draw_scene_event();

static void set_scene_viewport(int x, int y, int w, int h) {
  glViewport(x, y, w, h);
  scene_state.viewport.x = x;
  scene_state.viewport.y = y;
  scene_state.viewport.w = w;
  scene_state.viewport.h = h;
  queue_draw_scene_event();
};

static void set_scene_viewport_auto() {
  int w, h;
  SDL_GL_GetDrawableSize(win, &w, &h);
  SDL_GL_MakeCurrent(win, glctx);
  set_scene_viewport(0, 0, w, h);
};

static Uint32 update_scene(Uint32 interval, void *param) {
  if (((const struct scene_impl *)param)->update(&scene_state) != 0) {
    queue_draw_scene_event();
  };
  return interval;
};

static void init_scene() {
  SDL_GL_MakeCurrent(win, glctx);
  set_scene_viewport_auto();

  scene->init(&scene_state);

  scene_update_timer = SDL_AddTimer(10, update_scene, (void *)scene);
};

static void cleanup_scene() {
  SDL_GL_MakeCurrent(win, glctx);

  SDL_RemoveTimer(scene_update_timer);
  scene_update_timer = 0;

  scene->cleanup(&scene_state);
};

static void queue_draw_scene_event() {
  static SDL_UserEvent ev = {0, 0, 0, 0, NULL, NULL};
  ev.type = USEREVENT_DRAW_SCENE;
  /* FIXME? set timestamp? */               
  SDL_PushEvent((SDL_Event *)&ev);
};

static void draw_scene() {
  SDL_GL_MakeCurrent(win, glctx);
  scene->draw(&scene_state);
  SDL_GL_SwapWindow(win);
};

static void minimal_window_handler(SDL_Event *ev) {
  if (ev->type == SDL_WINDOWEVENT) {
    switch (ev->window.event) {
    case SDL_WINDOWEVENT_RESIZED:
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      set_scene_viewport_auto();
      /* fall through */
    case SDL_WINDOWEVENT_EXPOSED:
      queue_draw_scene_event();
      break;
    default:
      /* do nothing */;
    };
  };
};

static void minimal_key_handler(SDL_Event *ev) {
  if (ev->type == SDL_KEYDOWN) {
    switch (ev->key.keysym.sym) {
    case SDLK_ESCAPE:
    case SDLK_q:
      {
        SDL_QuitEvent quit_ev;
        quit_ev.type = SDL_QUIT;
        SDL_PushEvent((SDL_Event *)&quit_ev);
      };
      break;
    default:
      /* do nothing */;
    };
  };
};

typedef void (*event_handler)(SDL_Event *ev);
static const event_handler evhandler_map[128] = {
  NULL, /* unused 0x00 */
  NULL, /* app handler */               
  minimal_window_handler,
  minimal_key_handler,
  NULL, /* mouse handler */
  NULL, /* unused 0x05 */
  NULL, /* joystick/controller handler */
  NULL, /* raw touch input */
  NULL, /* touch gesture input */
  NULL, /* clipboard */
  NULL, /* unused 0x0A */
  NULL, /* unused 0x0B */
  NULL, /* unused 0x0C */
  NULL, /* unused 0x0D */
  NULL, /* unused 0x0E */
  NULL, /* unused 0x0F */
  NULL, /* drag-and-drop */
  NULL, /* audio device hotplug */
  NULL, /* unused 0x12 */
  NULL, /* unused 0x13 */
  NULL, /* unused 0x14 */
  NULL, /* unused 0x15 */
  NULL, /* unused 0x16 */
  NULL, /* unused 0x17 */
  NULL, /* unused 0x18 */
  NULL, /* unused 0x19 */
  NULL, /* unused 0x1A */
  NULL, /* unused 0x1B */
  NULL, /* unused 0x1C */
  NULL, /* unused 0x1D */
  NULL, /* unused 0x1E */
  NULL, /* unused 0x1F */
  NULL, /* renderer reset */
  NULL, /* unused 0x21 */
};

static void event_loop() {
  int i;
  SDL_Event event;

  while (1) {
    while (SDL_WaitEventTimeout(&event, 10)) {
      event_handler evh;

      if (event.type > 0xFFFF) {
	SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
		    "received event with type out of bounds");
	continue;
      };

      if (event.type >= 0x8000) {
        if (event.type == USEREVENT_DRAW_SCENE) {
          SDL_FlushEvent(USEREVENT_DRAW_SCENE);
          draw_scene();
        } else {
          SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                      "received unknown user event");
        };
      } else {
        evh = evhandler_map[event.type >> 8];
        if (evh != NULL) {
          evh(&event);
        };
      };

      if (event.type == SDL_QUIT) {
	SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION,
		    "received SDL_QUIT event; exiting event loop");
	return;
      };
    };
  };
};

int main(int argc, char *argv[]) {
  int rv = 0;

  if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO |
               SDL_INIT_GAMECONTROLLER) != 0) {

    util_log_sdl_error("SDL_Init");
    rv = 1;
    goto end;
  };

  /* FIXME move to event loop init function */                              
  USEREVENT_DRAW_SCENE = SDL_RegisterEvents(1);
  /* check assumptions used by event_loop */
  assert(SDL_USEREVENT == 0x8000);
  assert(SDL_LASTEVENT == 0xFFFF);

  win = SDL_CreateWindow("gltoy",
                         SDL_WINDOWPOS_UNDEFINED,
                         SDL_WINDOWPOS_UNDEFINED,
                         800, 450,
                         SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  if (win == NULL) {
    util_log_sdl_error("SDL_CreateWindow");
    rv = 1;
    goto end;
  };

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);

  glctx = SDL_GL_CreateContext(win);
  if (glctx == NULL) {
    util_log_sdl_error("SDL_GL_CreateContext");
    rv = 1;
    goto end;
  };

  if (db_init(NULL, "gltoy") < 0) goto end;
  asset_init("gltoy");

  if (check_texture_targets_arrays() < 0) goto end;
  register_all_texture_loaders();

  init_scene();

  event_loop();

 end:
  cleanup_scene();
  asset_quit();
  db_quit();

  if (glctx != NULL) {
    SDL_GL_DeleteContext(glctx);
    glctx = NULL;
  };

  if (win != NULL) {
    SDL_DestroyWindow(win);
    win = NULL;
  };

  SDL_Quit();
  exit(rv);
  return rv;
};
