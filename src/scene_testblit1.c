
#include "shader.h"
#include "texture.h"
#include "vecmat.h"
#include "scene.h"

#include <math.h>

#define ATTR_COLOR    0
#define ATTR_TEXRECT  1
#define ATTR_DISPPOS  2
#define ATTR_RECTPOS  3

static struct ShaderProgram *program_testblit1 = NULL;

static const GLfloat rect_positions[] = {
   0.0,  0.0,
   0.0, +1.0,
  +1.0,  0.0,
  +1.0, +1.0,
};
static const GLushort rect_tris_indices[] = {
  0, 1, 2,
  2, 1, 3,
};

static const f32v4 vec_color_white = { 1.0, 1.0, 1.0, 1.0 };

static void scene_init(struct scene_state *st) {
  program_testblit1 = load_shader_program_by_name("testblit1");
};

static int scene_update(struct scene_state *st) {
  return 0;
};

static inline float fracf(float x) {
  return x - floorf(x);
};

static f32v4 compute_center_alignment_offset(const struct scene_viewport *vp) {
  /* in x direction:
   *   physical drawing area is w=vp->w pixels
   *   GL drawing area is interval from -1.0 to 1.0
   *   GL will multiply gl_Position.x by a factor of w/2.0 (and add to it)
   *   need to return offset which, when subtracted from each vertex's gl_Position drawn,
   *       will map the left edge of a pixel near the center to x=0.0
   */
  const float center_x_physpixel = vp->w / (2.0);
  const float offset_x_physpixel = fracf(center_x_physpixel);
  const float offset_x_gl = offset_x_physpixel*2.0/(vp->w);
  /* and mutandis mutatis for y, to put a pixel's lower edge at 0.0 */
  const float center_y_physpixel = vp->h / (2.0);
  const float offset_y_physpixel = fracf(center_y_physpixel);
  const float offset_y_gl = offset_y_physpixel*2.0/(vp->h);
  const f32v4 rv = { offset_x_gl, offset_y_gl, 0.0, 0.0 };
  return rv;
};

static void scene_draw(struct scene_state *st) {
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT);

  use_program(program_testblit1);

  glEnableVertexAttribArray(ATTR_RECTPOS);
  glVertexAttribPointer(ATTR_RECTPOS, 2, GL_FLOAT, GL_FALSE, 0, rect_positions);

  glDisableVertexAttribArray(ATTR_COLOR);
  glVertexAttrib4fv(ATTR_COLOR, (const GLfloat *)&vec_color_white);

  program_set_uniform_texunit(program_testblit1, "tex0", GL_TEXTURE0);

  {
    struct Texture *tex = load_texture_by_name("forest_tiles");
    const struct scene_viewport const *vp = &(st->viewport);
    /*const GLfloat vTexRect[4] = { 0.0, 0.0, tex->width0, tex->height0 };*/   
    const GLfloat vTexRect[4] = { 128.0, 0.0, 32.0, 64.0 };
    const f32v4 texture_scale = { 1.0/(tex->width0), 1.0/(tex->height0), 0.0, 0.0 };
    const unsigned int pixel_chunkiness_scale_uint = 3;
    const float pixel_chunkiness_scale = pixel_chunkiness_scale_uint;
    const GLfloat vDispPos[3] = { -25.0, -15.0, 0.0 };
    const f32v4 display_scale = {
      2.0*pixel_chunkiness_scale/(vp->w), -2.0*pixel_chunkiness_scale/(vp->h),
      1.0/128.0, 1.0
    };
    f32v4 display_center = {
      0.0, 0.0,
      64.0, 0.0
    };

    display_center += compute_center_alignment_offset(vp) / display_scale;

    bind_texture(GL_TEXTURE0, tex);
    program_set_uniform_vec2(program_testblit1, "texture_scale", texture_scale);
    program_set_uniform_vec4(program_testblit1, "display_center", display_center);
    program_set_uniform_vec4(program_testblit1, "display_scale", display_scale);

    glDisableVertexAttribArray(ATTR_COLOR);
    glVertexAttrib4fv(ATTR_COLOR, (const GLfloat *)&vec_color_white);

    glDisableVertexAttribArray(ATTR_TEXRECT);
    glVertexAttrib4fv(ATTR_TEXRECT, vTexRect);

    glDisableVertexAttribArray(ATTR_DISPPOS);
    glVertexAttrib3fv(ATTR_DISPPOS, vDispPos);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, rect_tris_indices);
  };
};

static void scene_cleanup(struct scene_state *st) {
  /* TODO can't be implemented yet */
};

const struct scene_impl scene_testblit1 = {
  scene_init,
  scene_update,
  scene_draw,
  scene_cleanup,
};

