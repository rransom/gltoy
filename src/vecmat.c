
#include "vecmat.h"

#include <assert.h>
#include <math.h>
#include <string.h>

static void mat_float_zero(struct floatmat4 *out) {
  memset(out, 0, sizeof(*out));
};

void mat_float_identity(struct floatmat4 *out) {
  memset(out, 0, sizeof(*out));
  out->x[0][0] = out->x[1][1] = out->x[2][2] = out->x[3][3] = 1.0;
};

void mat_proj_frustum(struct floatmat4 *out, struct floatmat4 *outinv, float l, float r, float b, float t, float n, float f) {
  if (out != NULL) {
    const f32v4 col2 = { (r+l)/(r-l), (t+b)/(t-b), (n+f)/(n-f), -1.0 };
    mat_float_zero(out);
    out->x[0][0] = (2*n)/(r-l);
    out->x[1][1] = (2*n)/(t-b);
    out->x[2] = col2;
    out->x[3][2] = (2*n*f)/(n-f);
  };
  if (outinv != NULL) {
    const f32v4 col3 = { (r+l)/(2*n), (t+b)/(2*n), -1.0, (n+f)/(2*n*f) };
    mat_float_zero(outinv);
    outinv->x[0][0] = (r-l)/(2*n);
    outinv->x[1][1] = (t-b)/(2*n);
    outinv->x[2][3] = (n-f)/(2*n*f);
    outinv->x[3] = col3;
  };
};

void mat_proj_ortho(struct floatmat4 *out, struct floatmat4 *outinv, float l, float r, float b, float t, float n, float f) {
  if (out != NULL) {
    const f32v4 col3neg = { (r+l)/(r-l), (t+b)/(t-b), (n+f)/(n-f), -1.0 };
    mat_float_zero(out);
    out->x[0][0] = 2/(r-l);
    out->x[1][1] = 2/(t-b);
    out->x[2][2] = 2/(n-f);
    out->x[3] = -col3neg;
  };
  if (outinv != NULL) {
    const f32v4 col3 = { (r+l)/2, (t+b)/2, (n+f)/2, 1.0 };
    mat_float_zero(outinv);
    outinv->x[0][0] = (r-l)/2;
    outinv->x[1][1] = (t-b)/2;
    outinv->x[2][2] = (n-f)/2;
    outinv->x[3] = col3;
  };
};

void mat_proj_perspective(struct floatmat4 *out, struct floatmat4 *outinv, int fovaxis, float fovdeg, float aspect, float near, float far) {
  float fovaxis_ub;
  float half_width, half_height;
  float l, r, b, t;

  fovaxis_ub = tan((M_PI/180.0)*((double)fovdeg)/2)*near;

  switch (fovaxis) {
  default:
    /* no good way to report errors; just default to X axis */
  case AXIS_X:
    half_width = fovaxis_ub;
    half_height = half_width / aspect;
    break;
  case AXIS_Y:
    half_height = fovaxis_ub;
    half_width = half_height * aspect;
    break;
  };

  l = -half_width;  r =  half_width;
  b = -half_height; t =  half_height;

  mat_proj_frustum(out, outinv, l, r, b, t, near, far);
};

void mat_proj_perspective_rh(struct floatmat4 *out, struct floatmat4 *outinv, int fovaxis, float fovdeg, float aspect, float near, float far) {
  static const f32v4 mult = {1, 1, -1, 1};
  int i;
  mat_proj_perspective(out, outinv, fovaxis, fovdeg, aspect, near, far);
  for (i = 0; i < 4; ++i) {
    if (out    != NULL) out   ->x[i] *= mult;
    if (outinv != NULL) outinv->x[i] *= mult;
  };
};

void mat_float_translate(struct floatmat4 *out, struct floatmat4 *outinv, f32v4 offset) {
  offset[3] == 0.0;
  if (out    != NULL) out   ->x[3] += offset * out   ->x[3][3];
  if (outinv != NULL) outinv->x[3] += offset * outinv->x[3][3];
};

f32v4 quat_mult_float(f32v4 x, f32v4 y) {
  /* x = x[0]*i + x[1]*j + x[2]*k + x[3] */
  static const si32v4 shuf0 = { 3,  2,  1,  0};
  static const  f32v4 mult0 = { 1, -1,  1, -1};
  static const si32v4 shuf1 = { 2,  3,  0,  1};
  static const  f32v4 mult1 = { 1,  1, -1, -1};
  static const si32v4 shuf2 = { 1,  0,  3,  2};
  static const  f32v4 mult2 = {-1,  1,  1, -1};
  f32v4 rv = x[3] * y;
  rv += x[0] * __builtin_shuffle(y, shuf0) * mult0;
  rv += x[1] * __builtin_shuffle(y, shuf1) * mult1;
  rv += x[2] * __builtin_shuffle(y, shuf2) * mult2;
  return rv;
};

f32v4 quat_rescale(f32v4 x) {
  /* not a full normalization to magnitude 1; just avoid denormalization */
  int acc, tmp;
  frexpf(x[0], &acc);
  frexpf(x[1], &tmp); acc += tmp;
  frexpf(x[2], &tmp); acc += tmp;
  frexpf(x[3], &tmp); acc += tmp;
  return x*ldexpf(1.0, -(acc/4));
};

float quat_magsq_float(f32v4 x) {
  const f32v4 tmp = x*x;
  return tmp[0] + tmp[1] + tmp[2] + tmp[3];
};

f32v4 quat_rotate_quat_float(f32v4 quat, f32v4 vec) {
  static const f32v4 conjmult = {-1, -1, -1, 1};
  const f32v4 tmp1 = quat_mult_float(quat, vec);
  const f32v4 tmp2 = quat_mult_float(tmp1, quat*conjmult);
  return tmp2/quat_magsq_float(quat);
};

f32v4 quat_rotate_vector_float(f32v4 quat, f32v4 vec) {
  static const f32v4 vecmask = {1, 1, 1, 0};
  static const f32v4 vecscalemask = {0, 0, 0, 1};
  return quat_rotate_quat_float(quat, vec*vecmask) + vec*vecscalemask;
};

static int float_near_zero(float in) {
  const int c = fpclassify(in);
  return (c == FP_ZERO) || (c == FP_SUBNORMAL);
};

static int quat_imag_part_near_zero(f32v4 in) {
  return float_near_zero(in[AXIS_X]) && float_near_zero(in[AXIS_Y]) && float_near_zero(in[AXIS_Z]);
};

static f32v4 vec3_make_orthogonal_float(f32v4 in) {
  static const f32v4 I = {1, 0, 0, 0};
  static const f32v4 J = {0, 1, 0, 0};
  static const f32v4 K = {0, 0, 1, 0};
  f32v4 mult, rv;

  in[AXIS_W] = 0.0;

  /* find a vector which is not parallel to in, if possible */
  if (!float_near_zero(in[AXIS_X])) {
    mult = J;
  } else if (!float_near_zero(in[AXIS_Y])) {
    mult = K;
  } else if (!float_near_zero(in[AXIS_Z])) {
    mult = I;
  } else {
    /* in is the zero vector; every vector is orthogonal to it */
    return I;
  };

  /* compute the cross product with mult */
  rv = quat_mult_float(in, mult);
  rv[AXIS_W] = 0.0;

  return quat_rescale(rv);
};

static f32v4 quat_rotation_halve_angle_float(f32v4 in, f32v4 dest) {
  const float c = in[AXIS_W];
  const f32v4 insq = in*in;
  const float ssq = insq[AXIS_X] + insq[AXIS_Y] + insq[AXIS_Z];
  const float s = sqrtf(ssq);
  const float m = hypotf(c, s);
  f32v4 axis = in/s;
  float hc, hs;

  /* (hc, hs) is on the same line through the origin as the midpoint
     of the line segment between (c, s) and (m, 0).  Since a common
     factor is unimportant, we can use (hc, hs) = (c+m, s) unless c
     is near -m (which implies that s is small). */
  if (fabsf(s) < -0.0625*c) {
    /* Conceptually, factor 0 + J out of hc + hs*J, which
       corresponds to dividing c + s*J by -1.  Then hc + hs*J =
       ((m-c) - s*J)*J = s + (m-c)*J. */
    hc = s; hs = m-c;
  } else {
    hc = c+m; hs = s;
  };

  if (quat_imag_part_near_zero(in)) {
    float mag;
    axis = vec3_make_orthogonal_float(dest);
    /* axis[AXIS_W] is already zeroed */
    mag = sqrtf(quat_magsq_float(axis));
    axis /= mag;
  };

  axis[AXIS_W] = 0.0;

  {
    f32v4 rv = axis*hs;
    rv[AXIS_W] = hc;
    return quat_rescale(rv);
  };
};

f32v4 quat_frame_float(f32v4 dir, f32v4 up) {
  f32v4 rv = {0, 0, 0, 1};

  assert(dir[AXIS_W] == 0.0);
  assert(up [AXIS_W] == 0.0);

  /* rotate dir to K */
  {
    /* need any non-zero multiple of cos(theta/2) + sin(theta/2)*axis */

    static const f32v4 dest = {0, 0, 1, 0}; /* dest = K */
    const f32v4 tmp1 = quat_mult_float(dir, dest);

    /* tmp1 = -dot(dir, dest) + cross(dir, dest)
            = norm(dir)*norm(dest)*(-cos(theta) + sin(theta)*axis) */

    static const f32v4 mult = {1, 1, 1, -1};
    const f32v4 tmp2 = quat_rescale(tmp1*mult);

    /* tmp2 is a multiple of cos(theta) + sin(theta)*axis */

    const f32v4 rvpart = quat_rotation_halve_angle_float(tmp2, dest);

    /* rv = quat_mult_float(rvpart, rv); */ rv = rvpart;
  };

  /* rotate up now */
  up = quat_rotate_vector_float(rv, up);

  /* eliminate component of up along dir (now K) */
  up[AXIS_Z] = 0.0;

  /* rotate up to J */
  {
    static const f32v4 dest = {0, 1, 0, 0}; /* dest = J */
    const f32v4 tmp1 = quat_mult_float(up, dest);

    static const f32v4 mult = {1, 1, 1, -1};
    const f32v4 tmp2 = quat_rescale(tmp1*mult);

    const f32v4 rvpart = quat_rotation_halve_angle_float(tmp2, dest);

    rv = quat_mult_float(rvpart, rv);
  };

  return quat_rescale(rv);
};

f32v4 vec_normalize(f32v4 x) {
  float mag = sqrtf(quat_magsq_float(x));
  return x/mag;
};

void mat_rot_from_quat_float(struct floatmat4 *out, struct floatmat4 *outinv, f32v4 in) {
  static const f32v4 axes[] = {
    {1, 0, 0, 0},
    {0, 1, 0, 0},
    {0, 0, 1, 0},
    {0, 0, 0, 1},
  };
  static const f32v4 conjmult = {-1, -1, -1, 1};
  const f32v4 in_inv = in*conjmult;
  int i;

  for (i = 0; i < 4; ++i) {
    if (out    != NULL) out   ->x[i] = quat_rotate_quat_float(in    , axes[i]);
    if (outinv != NULL) outinv->x[i] = quat_rotate_quat_float(in_inv, axes[i]);
  };
};

void mat_from_quat_pos_float(struct floatmat4 *out, struct floatmat4 *outinv, f32v4 rotquat, f32v4 pos) {
  mat_rot_from_quat_float(out, outinv, rotquat);
  if (out    != NULL) out   ->x[3] -= pos;
  if (outinv != NULL) outinv->x[3] += pos;
};

void mat_mult_float(struct floatmat4 *out, const struct floatmat4 *a, const struct floatmat4 *b) {
  f32v4 t0 = a->x[0]*b->x[0][0] + a->x[1]*b->x[0][1] + a->x[2]*b->x[0][2] + a->x[3]*b->x[0][3];
  f32v4 t1 = a->x[0]*b->x[1][0] + a->x[1]*b->x[1][1] + a->x[2]*b->x[1][2] + a->x[3]*b->x[1][3];
  f32v4 t2 = a->x[0]*b->x[2][0] + a->x[1]*b->x[2][1] + a->x[2]*b->x[2][2] + a->x[3]*b->x[2][3];
  f32v4 t3 = a->x[0]*b->x[3][0] + a->x[1]*b->x[3][1] + a->x[2]*b->x[3][2] + a->x[3]*b->x[3][3];

  out->x[0] = t0; out->x[1] = t1; out->x[2] = t2; out->x[3] = t3;
};

void mat_copy_float(struct floatmat4 *out, const struct floatmat4 *in) {
  *out = *in;
};

