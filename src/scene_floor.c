
#include "shader.h"
#include "shader_asset.h"
#include "texture.h"
#include "texture_asset.h"
#include "vecmat.h"
#include "scene.h"
#include "model.h"

extern struct model model_nature063;
void model_nature063_init();

static struct shader_part shader_part_vertex = {
  shader_part_asset_loader,
  "shaders/floor-vertex.glsl",
  NULL
};

static struct shader_part shader_part_fragment = {
  shader_part_asset_loader,
  "shaders/floor-fragment.glsl",
  NULL
};

static struct shader_part *shader_vertex_parts[] = {
  &shader_part_vertex,
  NULL
};
static struct shader_spec shader_vertex = {
  GL_VERTEX_SHADER,
  "floor-vertex",
  shader_vertex_parts,
  0
};

static struct shader_part *shader_fragment_parts[] = {
  &shader_part_fragment,
  NULL
};
static struct shader_spec shader_fragment = {
  GL_FRAGMENT_SHADER,
  "showaxes-fragment",
  shader_fragment_parts,
  0
};

#define ATTR_POSITION  0
#define ATTR_TEXCOORD  2
#define ATTR_COLOR     1
#define ATTR_TANGENT_S 4
#define ATTR_TANGENT_T 5
static const struct attribute_binding attbind_vPosition  = {"vPosition", ATTR_POSITION };
static const struct attribute_binding attbind_vTexCoord  = {"vTexCoord", ATTR_TEXCOORD };
static const struct attribute_binding attbind_vColor     = {"vColor",    ATTR_COLOR    };
static const struct attribute_binding attbind_vTangentS  = {"vTangentS", ATTR_TANGENT_S};
static const struct attribute_binding attbind_vTangentT  = {"vTangentT", ATTR_TANGENT_T};
static const struct attribute_binding *attbinds[] = {
  &attbind_vPosition,
  &attbind_vTexCoord,
  &attbind_vColor,
  &attbind_vTangentS,
  &attbind_vTangentT,
  NULL
};

static struct shader_spec *shader_program_floor_shaders[] = {
  &shader_vertex,
  &shader_fragment,
  NULL
};
static struct program_spec shader_program_floor = {
  "floor",
  shader_program_floor_shaders,
  attbinds,
  0
};

static struct texture_part texture_part_white = {
  texture_part_asset_loader,
  "textures/white.webp",
  GL_TEXTURE_2D
};

static struct texture_part *texture_spec_white_parts[] = { &texture_part_white, NULL };
static struct texture_spec texture_spec_white = {
  GL_TEXTURE_2D,
  "white",
  texture_spec_white_parts,
  NULL
};

static struct texture_part texture_part_floor_diffuse = {
  texture_part_asset_loader,
  "textures/nieb/tile02.webp",
  GL_TEXTURE_2D
};
static struct texture_part *texture_spec_floor_diffuse_parts[] = { &texture_part_floor_diffuse, NULL };
static struct texture_spec texture_spec_floor_diffuse = {
  GL_TEXTURE_2D,
  "nieb/tile02",
  texture_spec_floor_diffuse_parts,
  NULL
};

static struct texture_part texture_part_floor_normal = {
  texture_part_asset_loader,
  "textures/nieb/tile02_normal.webp",
  GL_TEXTURE_2D
};
static struct texture_part *texture_spec_floor_normal_parts[] = { &texture_part_floor_normal, NULL };
static struct texture_spec texture_spec_floor_normal = {
  GL_TEXTURE_2D,
  "nieb/tile02_normal",
  texture_spec_floor_normal_parts,
  NULL
};

static struct texture_part texture_part_floor_specular = {
  texture_part_asset_loader,
  "textures/nieb/tile02_spec.webp",
  GL_TEXTURE_2D
};
static struct texture_part *texture_spec_floor_specular_parts[] = { &texture_part_floor_specular, NULL };
static struct texture_spec texture_spec_floor_specular = {
  GL_TEXTURE_2D,
  "nieb/tile02_spec",
  texture_spec_floor_specular_parts,
  NULL
};

static struct texture *texture_white = NULL;
static struct texture *texture_floor_diffuse  = NULL;
static struct texture *texture_floor_normal   = NULL;
static struct texture *texture_floor_specular = NULL;

static const GLfloat floor_position[] = {
  /* FIXME check ordering */               
  -64.0, -64.0, 0.0, 1.0,
  -64.0, +64.0, 0.0, 1.0,
  +64.0, -64.0, 0.0, 1.0,
  +64.0, +64.0, 0.0, 1.0,
};
static const GLfloat floor_texcoord[] = {
  /* FIXME check ordering */               
  -16.0, -16.0,
  -16.0, +16.0,
  +16.0, -16.0,
  +16.0, +16.0,
};
static const GLfloat floor_color_const[4] = { 1.0, 1.0, 1.0, 1.0 };
static const GLfloat floor_tangent_s_const[4] = { 4.0, 0.0, 0.0, 1.0 };
static const GLfloat floor_tangent_t_const[4] = { 0.0, 4.0, 0.0, 1.0 };
static const GLushort floor_indices[] = {
  /* for GL_TRIANGLE_STRIP */
  0, 1, 2, 3,
};

static struct floatmat4 mat_transform_pv, mat_transform_m;
static struct floatmat4 mat_proj_persp;
static f32v4 quat_view_fp, vec_view_fp_dir, vec_view_fp_up, vec_view_fp_pos;
static struct floatmat4 mat_view_fp;

static struct floatmat4 mat_transform_tree_m, mat_transform_tree_m_normal;
static struct floatmat4 mat_transform_tree_pvm;
static GLfloat mat_transform_tree_m_normal_uniform[9];
static f32v4 vec_tree_pos;

static const f32v4 vec_tree_pos_init = { -1.5, 8.0, 2.0, 0.0 };

static const f32v4 vec_view_fp_dir_init = { 0.0, 1.0, -0.3, 0.0 };
static const f32v4 vec_view_fp_up_init  = { 0.0, 0.0, 1.0, 0.0 };
static const f32v4 vec_view_fp_pos_init = { 0.0, 0.0, 8.0, 0.0 };

static const f32v4 vec_light_dir_source = { 1.0, 1.0, 1.0, 0.0 };
static const f32v4 vec_light_dir_color = { 0.9, 0.9, 0.9, 1.0 };
static const f32v4 vec_light_ambient_color = { 0.125, 0.125, 0.125, 1.0 };

static const f32v4 vec_texcoord_dummy = { 0.0, 0.0, 0.0, 1.0 };
static const f32v4 vec_color_white = { 1.0, 1.0, 1.0, 1.0 };
static const f32v4 vec_color_red = { 1.0, 0.0, 0.0, 1.0 };
static const f32v4 vec_color_green = { 0.0, 1.0, 0.0, 1.0 };

static struct program_uniform_binding tree_uniform_transform_pvm = {
  "transform_pvm",
  GL_FLOAT_MAT4,
  1,
  &mat_transform_tree_pvm,
  -1
};
static struct program_uniform_binding tree_uniform_transform_m = {
  "transform_m",
  GL_FLOAT_MAT4,
  1,
  &mat_transform_tree_m,
  -1
};
static struct program_uniform_binding tree_uniform_transform_m_normal = {
  "transform_m_normal",
  GL_FLOAT_MAT3,
  1,
  mat_transform_tree_m_normal_uniform,
  -1
};

static struct program_uniform_binding scene_uniform_light_dir_source = {
  "light_dir_source",
  GL_FLOAT_VEC3,
  1,
  &vec_light_dir_source,
  -1
};
static struct program_uniform_binding scene_uniform_light_dir_color = {
  "light_dir_color",
  GL_FLOAT_VEC4,
  1,
  &vec_light_dir_color,
  -1
};
static struct program_uniform_binding scene_uniform_light_ambient_color = {
  "light_ambient_color",
  GL_FLOAT_VEC4,
  1,
  &vec_light_ambient_color,
  -1
};
static struct program_uniform_binding scene_uniform_eye_position = {
  "eye_position",
  GL_FLOAT_VEC3,
  1,
  &vec_view_fp_pos,
  -1
};

static struct program_uniform_binding *tree_uniforms[] = {
  &tree_uniform_transform_pvm,
  &tree_uniform_transform_m,
  &tree_uniform_transform_m_normal,
  &scene_uniform_light_dir_source,
  &scene_uniform_light_dir_color,
  &scene_uniform_light_ambient_color,
  &scene_uniform_eye_position,
  NULL
};

static void fprintvec_float(FILE *f, const char *name, f32v4 v) {
  if (name != NULL) fprintf(f, "%s: ", name);
  fprintf(f, "[%f, %f, %f, %f]\n", v[0], v[1], v[2], v[3]);
};

static void fprintmat_float(FILE *f, const char *name, const struct floatmat4 *m) {
  int i;
  if (name != NULL) fprintf(f, "%s (transposed):\n", name);
  for (i = 0; i < 4; ++i) {
    const f32v4 v = m->x[i];
    fprintf(f, "  [%f, %f, %f, %f]\n", v[0], v[1], v[2], v[3]);
  };
};

static void scene_init(struct scene_state *st) {
  float aspect;

  model_nature063_init();

  texture_white = load_texture(&texture_spec_white);
  texture_floor_diffuse  = load_texture(&texture_spec_floor_diffuse );
  texture_floor_normal   = load_texture(&texture_spec_floor_normal  );
  texture_floor_specular = load_texture(&texture_spec_floor_specular);

  program_set_uniform_texunit(&shader_program_floor, "tex_diffuse",  GL_TEXTURE0);
  program_set_uniform_texunit(&shader_program_floor, "tex_normal",   GL_TEXTURE1);
  program_set_uniform_texunit(&shader_program_floor, "tex_specular", GL_TEXTURE2);

  program_set_uniform_float(&shader_program_floor, "specular_exponent", 16.0);

  aspect = ((float)st->viewport_w)/((float)st->viewport_h);
  mat_proj_perspective(&mat_proj_persp, NULL, AXIS_X, 90.0, aspect, 1.0, 256.0);

  vec_view_fp_dir = vec_view_fp_dir_init;
  vec_view_fp_up  = vec_view_fp_up_init;
  vec_view_fp_pos = vec_view_fp_pos_init;

  quat_view_fp = quat_frame_float(vec_view_fp_dir, vec_view_fp_up);
  mat_from_quat_pos_float(&mat_view_fp, NULL, quat_view_fp, vec_view_fp_pos);

  mat_mult_float(&mat_transform_pv, &mat_proj_persp, &mat_view_fp);

  vec_tree_pos = vec_tree_pos_init;

  mat_float_identity(&mat_transform_tree_m);
  mat_float_identity(&mat_transform_tree_m_normal);
  mat_float_translate(&mat_transform_tree_m, &mat_transform_tree_m_normal, vec_tree_pos);

  mat_mult_float(&mat_transform_tree_pvm, &mat_transform_pv, &mat_transform_tree_m);
  program_mat3_transpose_and_trim(mat_transform_tree_m_normal_uniform, &mat_transform_tree_m_normal);

  model_prepare_parameters(&model_nature063, tree_uniforms);
};

static int scene_update(struct scene_state *st) {
  return 1;
};

static void scene_draw(struct scene_state *st) {
  struct program_spec *prog = &shader_program_floor;

  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClearDepthf(1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS); /* default in GLES 2.0; GL is left-handed */

  mat_float_identity(&mat_transform_m);

  use_program(prog);
  program_set_uniform_mat4(prog, "transform_pvm", &mat_transform_pv);
  program_set_uniform_mat3_transposed(prog, "transform_m", &mat_transform_m);

  program_set_uniform_vec3(prog, "light_dir_source", vec_light_dir_source);
  program_set_uniform_vec4(prog, "light_dir_color", vec_light_dir_color);
  program_set_uniform_vec4(prog, "light_ambient_color", vec_light_ambient_color);

  program_set_uniform_vec3(prog, "eye_position", vec_view_fp_pos);

  bind_texture(GL_TEXTURE0, texture_floor_diffuse );
  bind_texture(GL_TEXTURE1, texture_floor_normal  );
  bind_texture(GL_TEXTURE2, texture_floor_specular);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  glEnableVertexAttribArray(ATTR_POSITION);
  glEnableVertexAttribArray(ATTR_TEXCOORD);
  glDisableVertexAttribArray(ATTR_COLOR);
  glDisableVertexAttribArray(ATTR_TANGENT_S);
  glDisableVertexAttribArray(ATTR_TANGENT_T);

  glVertexAttribPointer(ATTR_POSITION, 4, GL_FLOAT, GL_FALSE, 0, floor_position);
  glVertexAttribPointer(ATTR_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, floor_texcoord);
  glVertexAttrib4fv(ATTR_COLOR    , floor_color_const);
  glVertexAttrib4fv(ATTR_TANGENT_S, floor_tangent_s_const);
  glVertexAttrib4fv(ATTR_TANGENT_T, floor_tangent_t_const);

  glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_SHORT, floor_indices);

  model_draw(&model_nature063, tree_uniforms);
};

static void scene_cleanup(struct scene_state *st) {
  /* TODO can't be implemented yet */
};

const struct scene_impl scene_floor = {
  scene_init,
  scene_update,
  scene_draw,
  scene_cleanup,
};

