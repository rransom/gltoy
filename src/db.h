#ifndef Xdih3d9x48fjc69irptp7mxghcb3lwt14kpylggj3w49cm80e4pz0xlv2uy3bysje
#define Xdih3d9x48fjc69irptp7mxghcb3lwt14kpylggj3w49cm80e4pz0xlv2uy3bysje

#include <assert.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>

#include <SDL.h>

#include <sqlite3.h>

void db_vreportf_error(int errcode, const char *ctx, va_list ap);
void db_reportf_error(int errcode, const char *ctx, ...);
void db_report_error(const char *ctx, int errcode);

int db_init(const char *orgname_, const char *appname_);
void db_quit();

struct db_static_query {
  struct db_static_query *next;
  sqlite3_stmt *stmt;
  const char *name;
  const char *sql;
  int state;
  int query_failed;
};

#define DBSQ_STATE_UNINITIALIZED 0
#define DBSQ_STATE_INACTIVE 1
#define DBSQ_STATE_STARTED 2
#define DBSQ_STATE_STEPPED 3

#define DB_STATIC_QUERY_STMT(name, query) \
  static struct db_static_query name = { NULL, NULL, #name, query, DBSQ_STATE_UNINITIALIZED, 0 }

int dbsq_start(struct db_static_query *dbsq);

void dbsq_bind_null(struct db_static_query *dbsq, int paramindex);
void dbsq_bind_text(struct db_static_query *dbsq, int paramindex, const char *value);
void dbsq_bind_int64(struct db_static_query *dbsq, int paramindex, int64_t value);

int dbsq_step(struct db_static_query *dbsq);

void dbsq_done(struct db_static_query *dbsq);

#define DBSQ_CLIENT_VARDEFS \
  struct db_static_query *cur_dbsq = NULL

#define DBSQ_CLIENT_ERRLABEL err

#define DBSQ_START(query) {                                     \
    assert(cur_dbsq == NULL);                                   \
    if (dbsq_start(&(query)) < 0) goto DBSQ_CLIENT_ERRLABEL;    \
    cur_dbsq = &(query);                                        \
  }

#define DBSQ_BIND_NULL(paramindex) dbsq_bind_null(cur_dbsq, paramindex)
#define DBSQ_BIND_TEXT(paramindex, value) dbsq_bind_text(cur_dbsq, paramindex, value)
#define DBSQ_BIND_INT64(paramindex, value) dbsq_bind_int64(cur_dbsq, paramindex, value)

#define DBSQ_CHECK_FOR_BIND_FAILURE {           \
    if (cur_dbsq->query_failed) {               \
      goto DBSQ_CLIENT_ERRLABEL;                \
    };                                          \
  }

#define DBSQ_STEP dbsq_step(cur_dbsq)

#define DBSQ_DONE {                             \
    assert(cur_dbsq != NULL);                   \
    dbsq_done(cur_dbsq);                        \
    cur_dbsq = NULL;                            \
  }

#define DBSQ_CLEANUP_AFTER_ERR {                \
    if (cur_dbsq != NULL) {                     \
      dbsq_done(cur_dbsq);                      \
      cur_dbsq = NULL;                          \
    };                                          \
  }

#endif
