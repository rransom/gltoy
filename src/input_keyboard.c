
#include "input_impl.h"

typedef uint16_t key_dispatch_entry_index;
struct key_dispatch_entry {
  Uint16 mod_ub;
  Uint16 mod_lb;
  uint16_t bindflags;
  struct input_effect effect;
};

static int check_mod_bounds(Uint16 mod, Uint16 mod_ub, Uint16 mod_lb) {
  int fits_ub = (mod & mod_ub) == mod;
  int fits_lb = (mod | mod_lb) == mod;
  return fits_ub & fits_lb;
};

#define KEY_DISPATCH_TABLE_LEVEL_TYPE_MIDLEVEL 1
#define KEY_DISPATCH_TABLE_LEVEL_TYPE_BASE     2

typedef uint8_t key_dispatch_table_level_index;
#define MAX_KEY_DISPATCH_TABLE_LEVEL 64
#define KEY_DISPATCH_TABLE_LEVEL_DEGREE 256
#define KEY_DISPATCH_TABLE_LEVEL_DEGREE_LOG 8
#define KEY_DISPATCH_TABLE_LEVEL_DEGREE_MASK 255

struct key_dispatch_table_level_midlevel {
  uint16_t type;
  key_dispatch_table_level_index child_index[KEY_DISPATCH_TABLE_LEVEL_DEGREE];
};
struct key_dispatch_table_level_base {
  uint16_t type;
  key_dispatch_entry_index entry_index[KEY_DISPATCH_TABLE_LEVEL_DEGREE];
};

static void *key_dispatch_table_levels[MAX_KEY_DISPATCH_TABLE_LEVEL + 1];
static key_dispatch_table_level_index key_dispatch_table_level_next = 1;











