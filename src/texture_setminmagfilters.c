
#include <SDL.h>

#include <assert.h>
#include <string.h>
#include <bsd/string.h>

#include "texture.h"

struct constmap_entry {
  const char *name;
  GLenum value;
};
static const struct constmap_entry filtertype_map[] = {
  {"nearest", GL_NEAREST},
  {"linear", GL_LINEAR},
  {"nearest_mipmap_nearest", GL_NEAREST_MIPMAP_NEAREST},
  {"nearest_mipmap_linear", GL_NEAREST_MIPMAP_LINEAR},
  {"linear_mipmap_nearest", GL_LINEAR_MIPMAP_NEAREST},
  {"linear_mipmap_linear", GL_LINEAR_MIPMAP_LINEAR},
  {NULL, 0}
};
static GLenum lookup_filtertype(const char *name) {
  const struct constmap_entry *p;
  for (p = filtertype_map; p->name != NULL; ++p) {
    if (strcmp(name, p->name) == 0) break;
  };
  return p->value;
};
#define MAX_FILTERTYPE_NAME_LEN 80

static int setminmagfilters_loader_func(struct TextureLoaderArgs *args) {
  const char *p;
  size_t len;
  char filtertype[MAX_FILTERTYPE_NAME_LEN+1];

  p = strchr(args->loader_opts, ',');
  len = (p == NULL) ? args->loader_opts_len : (p - args->loader_opts);
  if (len > MAX_FILTERTYPE_NAME_LEN) len = MAX_FILTERTYPE_NAME_LEN;
  memcpy(filtertype, args->loader_opts, len);
  filtertype[len] = '\0';

  glTexParameteri(args->target->bind_target, GL_TEXTURE_MIN_FILTER, lookup_filtertype(filtertype));

  if (p != NULL) {
    assert((*p) == ',');
    ++p;
    len = strlen(p);
    if (len > MAX_FILTERTYPE_NAME_LEN) len = MAX_FILTERTYPE_NAME_LEN;
    memcpy(filtertype, p, len);
    filtertype[len] = '\0';
  }; /* else use same filter for mag */

  glTexParameteri(args->target->bind_target, GL_TEXTURE_MAG_FILTER, lookup_filtertype(filtertype));
};

void register_texture_loader_setminmagfilters() {
  register_texture_image_loader_func("setminmagfilters", setminmagfilters_loader_func, 0);
};

