
#include "objmap.h"

static int objmap_idquery_by_dbid(struct objmap *map, int64_t dbid) {
  int rv;

  if (dbsq_start(map->idquery) < 0) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                 "failed to prepare idquery for struct %s objmap",
                 map->structname);
    goto err;
  };

  dbsq_bind_int64(map->idquery, 1, dbid);
  dbsq_bind_null(map->idquery, 2);

  if (map->idquery->query_failed) return -1; /* FIXME */     

  return 0;

 err:
  dbsq_done(map->idquery);
  /* FIXME fatal error */               
  return -2;
};

static int objmap_idquery_by_name(struct objmap *map, const char *name) {
  int rv;

  if (dbsq_start(map->idquery) < 0) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                 "failed to prepare idquery for struct %s objmap",
                 map->structname);
    goto err;
  };

  dbsq_bind_null(map->idquery, 1);
  dbsq_bind_text(map->idquery, 2, name);

  if (map->idquery->query_failed) return -1; /* FIXME */     

  return 0;

 err:
  dbsq_done(map->idquery);
  /* FIXME fatal error */               
  return -2;
};

static int objmap_idquery_fill_entry_head(struct objmap *map, struct objmap_entry_head *head) {
  if (!dbsq_step(map->idquery)) return -1; /* no such object found */

  head->dbid = sqlite3_column_int64(map->idquery->stmt, 0);
  head->dbname = strdup(sqlite3_column_text(map->idquery->stmt, 1));

  return 0;
};

int objmap_cmpfn_byid(void *a_, void *b_) {
  struct objmap_entry_head *a = a_;
  struct objmap_entry_head *b = b_;

  if (a->dbid < b->dbid) {
    return -1;
  } else if (a->dbid == b->dbid) {
    return 0;
  } else /* (a->dbid > b->dbid) */ {
    return 1;
  };
};

int objmap_cmpfn_byname(void *a_, void *b_) {
  struct objmap_entry_head *a = a_;
  struct objmap_entry_head *b = b_;

  return strcmp(a->dbname, b->dbname);
};

static void objmap_ensure_inited(struct objmap *map) {
  if (map->byid == NULL) {
    map->byid = newtree234(objmap_cmpfn_byid);
  };
  if (map->byname == NULL) {
    map->byname = newtree234(objmap_cmpfn_byname);
  };
};

void *objmap_finish_load(struct objmap *map) {
  void *obj = NULL;

  obj = map->alloc();
  if (obj == NULL) return NULL;

  if (objmap_idquery_fill_entry_head(map, obj) < 0) goto err;

  /* assumes DB id field and name field are each UNIQUE NOT NULL */
  add234(map->byid, obj);
  add234(map->byname, obj);

  if (map->loader(obj) < 0) {
    SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                "%s object loader func reported failure loading %s (%Ld)",
                map->structname,
                ((struct objmap_entry_head *)obj)->dbname,
                (long long int)(((struct objmap_entry_head *)obj)->dbid));
  };

  return obj;

 err:
  map->dtor(obj);
  return NULL;
};

void *objmap_load_by_name(struct objmap *map, const char *dbname) {
  struct objmap_entry_head cmpkey = {0, (char *)dbname};
  void *obj = NULL;

  objmap_ensure_inited(map);

  obj = find234(map->byname, &cmpkey, NULL);
  if (obj == NULL) {
    if (objmap_idquery_by_name(map, dbname) < 0) return NULL;
    obj = objmap_finish_load(map);
  };

  return obj;
};

void *objmap_load_by_dbid(struct objmap *map, int64_t dbid) {
  struct objmap_entry_head cmpkey = {dbid, NULL};
  void *obj = NULL;

  objmap_ensure_inited(map);

  obj = find234(map->byid, &cmpkey, NULL);
  if (obj == NULL) {
    if (objmap_idquery_by_dbid(map, dbid) < 0) return NULL;
    obj = objmap_finish_load(map);
  };

  return obj;
};

