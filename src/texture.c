
#include <SDL.h>

#include "texture.h"

#include "db.h"
#include "objmap.h"

#include "tree234.h"

static const struct TextureImageTarget texture_image_targets[] = {
  { "2d", GL_TEXTURE_2D, GL_TEXTURE_2D },
  { NULL, 0, 0 },
  { "cube_map_positive_x", GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP_POSITIVE_X },
  { "cube_map_negative_x", GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP_NEGATIVE_X },
  { "cube_map_positive_y", GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP_POSITIVE_Y },
  { "cube_map_negative_y", GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y },
  { "cube_map_positive_z", GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP_POSITIVE_Z },
  { "cube_map_negative_z", GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z },
  { NULL, 0, 0 }
};
static const struct TextureTarget texture_targets[] = {
  { "2d", 1, &(texture_image_targets[0]), GL_TEXTURE_2D, GL_TEXTURE_BINDING_2D },
  { "cube_map", 6, &(texture_image_targets[2]), GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BINDING_CUBE_MAP },
  { NULL, 0, NULL, 0, 0 }
};

#define N_STATIC_ARRAY_ELEMENTS(arr) (sizeof(arr)/sizeof(arr[0]))

int check_texture_targets_arrays() {
  int err_found = 0;
  size_t i, j;

  /* check that texture_targets is non-empty */
  i = N_STATIC_ARRAY_ELEMENTS(texture_targets);
  if (i == 0) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                 "texture_targets array is empty?!");
    goto err;
  };

  /* check that texture_targets has a sentinel at the end */
  i = N_STATIC_ARRAY_ELEMENTS(texture_targets) - 1;
  if ((texture_targets[i].name != NULL) ||
      (texture_targets[i].n_image_targets != 0) ||
      (texture_targets[i].image_targets != NULL) ||
      (texture_targets[i].bind_target != 0) ||
      (texture_targets[i].binding_query_parameter != 0)) {

    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                 "last element of texture_targets array is not a sentinel");
    goto err;
  };

  /* check that each texture_targets element is sane and has a sane image_targets list */
  for (i = 0; i < N_STATIC_ARRAY_ELEMENTS(texture_targets) - 1; ++i) {
    const struct TextureTarget *target = &(texture_targets[i]);
    const char *target_name = target->name;

    if (target->name == NULL) {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                   "texture_targets array element %zd has NULL name",
                   i);
      err_found = 1;
      target_name = "(NULL!)";
    };

    if ((target->bind_target == 0) ||
        (target->binding_query_parameter == 0)) {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                   "texture_targets array element %zd (%s) has a zero GLenum value",
                   i, target_name);
      err_found = 1;
    };

    if (target->image_targets == NULL) {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                   "texture_targets array element %zd (%s) has NULL image_targets list",
                   i, target_name);
      err_found = 1;
      continue;
    };

    for (j = 0; j < target->n_image_targets; ++j) {
      const struct TextureImageTarget *imgtarget = &(target->image_targets[j]);
      const char *imgtarget_name = imgtarget->name;

      if (imgtarget->name == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "target %zd (%s) image_targets array element %zd has NULL name",
                     i, target_name, j);
        err_found = 1;
        imgtarget_name = "(NULL!)";
      };

      if (imgtarget->texture_bind_target != target->bind_target) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "target %zd (%s) image_targets array element %zd (%s) has wrong texture_bind_target",
                     i, target_name, j, imgtarget_name);
        err_found = 1;
      };

      if (imgtarget->teximage_target == 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "target %zd (%s) image_targets array element %zd (%s) has zero teximage_target",
                     i, target_name, j, imgtarget_name);
        err_found = 1;
      };
    };

    if ((target->image_targets[target->n_image_targets].name != NULL) ||
        (target->image_targets[target->n_image_targets].texture_bind_target != 0) ||
        (target->image_targets[target->n_image_targets].teximage_target != 0)) {

      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                   "image_targets array for target %s is not followed by a sentinel",
                   target->name);
      err_found = 1;
    };
  };

  if (err_found) goto err;

  return 0;

 err:
  return -1;
};

const struct TextureTarget *lookup_texture_target_by_name(const char *name) {
  size_t i;
  for (i = 0; i < N_STATIC_ARRAY_ELEMENTS(texture_targets); ++i) {
    if ((texture_targets[i].name != NULL) &&
        (strcmp(name, texture_targets[i].name) == 0)) {

      return &(texture_targets[i]);
    };
  };
};

const struct TextureImageTarget *lookup_texture_image_target_by_name(const char *name) {
  size_t i;
  for (i = 0; i < N_STATIC_ARRAY_ELEMENTS(texture_image_targets); ++i) {
    if ((texture_image_targets[i].name != NULL) &&
        (strcmp(name, texture_image_targets[i].name) == 0)) {

      return &(texture_image_targets[i]);
    };
  };
};

struct TextureBindingSaver {
  const struct TextureTarget *target;
  GLuint old_globj;
};
static void bind_texture_and_save(struct TextureBindingSaver *saveslot, const struct TextureTarget *target, GLuint obj) {
  saveslot->target = target;
  saveslot->old_globj = 0;
  glGetIntegerv(target->binding_query_parameter, &(saveslot->old_globj));
  glBindTexture(target->bind_target, obj);
};
static void restore_texture_binding(struct TextureBindingSaver *saveslot) {
  glBindTexture(saveslot->target->bind_target, saveslot->old_globj);
};

struct TextureLoader {
  const char *name;
  texture_image_loader_func func;
  int struct_version;
};

static int TextureLoader_cmpfn_byname(void *a_, void *b_) {
  struct TextureLoader *a = a_;
  struct TextureLoader *b = b_;
  return strcmp(a->name, b->name);
};

static tree234 *loader_list_ = NULL;

static tree234 *get_loader_list() {
  if (loader_list_ == NULL) loader_list_ = newtree234(TextureLoader_cmpfn_byname);
  return loader_list_;
};

void register_texture_image_loader_func(const char *name, texture_image_loader_func func, int struct_version) {
  struct TextureLoader *loader = calloc(1, sizeof(struct TextureLoader));
  tree234 *loader_list = get_loader_list();
  void *rv;

  loader->name = name;
  loader->func = func;
  loader->struct_version = struct_version;

  rv = add234(loader_list, loader);
  if (rv != loader) {
    free(loader);
    loader = rv;
    loader->name = name;
    loader->func = func;
    loader->struct_version = struct_version;
  };
};

static const struct TextureLoader *lookup_texture_image_loader_func(const char *name) {
  struct TextureLoader dummy = {name, NULL, -1};
  return find234(get_loader_list(), &dummy, NULL);
};

OBJMAP_IMPL(Texture, texture,
  "SELECT tid, tname FROM assets.texture WHERE tid = ?1 OR tname = ?2",
  GLuint globj;
  const struct TextureTarget *target;
  GLsizei width0;
  GLsizei height0;
);

DB_STATIC_QUERY_STMT(texture_get_target,
  "SELECT ttarget FROM assets.texture "
  "  WHERE tid = ?1"
);

DB_STATIC_QUERY_STMT(texture_load_images,
  "SELECT timgid, timgloadorder, timglevel, timgtarget, timgloadername, timgloaderopts, bdata "
  "    FROM assets.textureimage "
  "    LEFT JOIN assets.datablob USING (bid) "
  "  WHERE tid = ?1"
  "  ORDER BY timgloadorder"
);

static int ensure_texture_loaded(void *obj_) {
  struct Texture *obj = obj_;
  struct TextureBindingSaver saveslot;
  const struct TextureTarget *target = NULL;
  DBSQ_CLIENT_VARDEFS;

  if (obj->globj != 0) return 0;

  DBSQ_START(texture_get_target);

  DBSQ_BIND_INT64(1, obj->base.dbid);
  DBSQ_CHECK_FOR_BIND_FAILURE;

  if (DBSQ_STEP) {
    const char *ttarget = sqlite3_column_text(texture_get_target.stmt, 0);

    obj->target = lookup_texture_target_by_name(ttarget);

    if (obj->target == NULL) {
      SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                  "texture %Ld (%s) has unsupported bind target %s",
                  (long long int)obj->base.dbid, obj->base.dbname, ttarget);
      goto err;
    };

    glGenTextures(1, &(obj->globj));
    bind_texture_and_save(&saveslot, obj->target, obj->globj);
  } else {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                 "no rows found in database for texture %Ld (%s)",
                 (long long int)obj->base.dbid, obj->base.dbname);
    goto err;
  };

  DBSQ_DONE;

  DBSQ_START(texture_load_images);

  DBSQ_BIND_INT64(1, obj->base.dbid);
  DBSQ_CHECK_FOR_BIND_FAILURE;

  while (DBSQ_STEP) {
    struct TextureLoaderArgs loaderargs = {0};
    const struct TextureLoader *loader;

    loaderargs.struct_version = 0;
    loaderargs.tex = obj;
    loaderargs.tex_globj = obj->globj;
    loaderargs.target = obj->target;
    loaderargs.image_target_name = sqlite3_column_text(texture_load_images.stmt, 3);
    loaderargs.image_target = lookup_texture_image_target_by_name(loaderargs.image_target_name);
    loaderargs.tex_imgid = sqlite3_column_int64(texture_load_images.stmt, 0);
    loaderargs.load_order = sqlite3_column_int64(texture_load_images.stmt, 1);
    loaderargs.level = sqlite3_column_int(texture_load_images.stmt, 2);
    loaderargs.loader_name = sqlite3_column_text(texture_load_images.stmt, 4);
    loaderargs.loader_opts = sqlite3_column_text(texture_load_images.stmt, 5);
    loaderargs.loader_opts_len = sqlite3_column_bytes(texture_load_images.stmt, 5);
    loaderargs.data = sqlite3_column_blob(texture_load_images.stmt, 6);
    loaderargs.data_len = sqlite3_column_bytes(texture_load_images.stmt, 6);
    if (loaderargs.level == 0) {
      loaderargs.tex_width_out = &(obj->width0);
      loaderargs.tex_height_out = &(obj->height0);
    } else {
      loaderargs.tex_width_out = NULL;
      loaderargs.tex_height_out = NULL;
    };

    loader = lookup_texture_image_loader_func(loaderargs.loader_name);

    if (loader == NULL) {
      SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                  "unknown image loader %s required for texture %Ld (%s) image %Ld",
                  loaderargs.loader_name,
                  (long long int)obj->base.dbid, obj->base.dbname,
                  (long long int)loaderargs.tex_imgid);
      goto err;
    };

    if (loader->func(&loaderargs) < 0) {
      SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                  "image loader %s reported failure loading texture %Ld (%s) image %Ld",
                  loaderargs.loader_name,
                  (long long int)obj->base.dbid, obj->base.dbname,
                  (long long int)loaderargs.tex_imgid);
      goto err;
    };
  };

  DBSQ_DONE;

  restore_texture_binding(&saveslot);

  return 0;

 err:
  texture_cleanup(obj);
  DBSQ_CLEANUP_AFTER_ERR;

  SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
              "failed to load texture %Ld (%s)",
              (long long int)obj->base.dbid, obj->base.dbname);

  return -1;
};

static void texture_cleanup(struct Texture *obj) {
  if (obj->globj != 0) {
    glDeleteTextures(1, &(obj->globj));
    obj->globj = 0;
  };
};

void bind_texture(GLenum unit, struct Texture *tex) {
  glActiveTexture(unit);
  glBindTexture(tex->target->bind_target, tex->globj);
};

void texture_binding_make_active(struct texture_binding *binding) {
  bind_texture(binding->texunit, binding->tex);
};

void texture_bindings_make_active(struct texture_binding **bindings) {
  if (bindings == NULL) return;
  while (*bindings != NULL) {
    texture_binding_make_active(*bindings);
    ++bindings;
  };
};

