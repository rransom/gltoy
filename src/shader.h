#ifndef Xbili7emidlc4j75lwbuzbuck32u2orf3iow7x8ouu3t9i9litwmmtqwqhkrbg5w3
#define Xbili7emidlc4j75lwbuzbuck32u2orf3iow7x8ouu3t9i9litwmmtqwqhkrbg5w3

#include <SDL_opengles2.h>

#include "vecmat.h"

struct ShaderProgram;

struct ShaderProgram *load_shader_program_by_name(const char *name);

void use_program(struct ShaderProgram *shp);

void program_set_uniform_texunit(struct ShaderProgram *shp, const char *param_name, GLenum texunit);
void program_set_uniform_float(struct ShaderProgram *shp, const char *param_name, GLfloat value);
void program_set_uniform_mat4(struct ShaderProgram *shp, const char *param_name, const struct floatmat4 *mat);
void program_set_uniform_mat3_transposed(struct ShaderProgram *shp, const char *param_name, const struct floatmat4 *mat);
void program_set_uniform_vec4(struct ShaderProgram *shp, const char *param_name, const f32v4 vec);
void program_set_uniform_vec3(struct ShaderProgram *shp, const char *param_name, const f32v4 vec);
void program_set_uniform_vec2(struct ShaderProgram *shp, const char *param_name, const f32v4 vec);

void program_set_attrib_const_floatvec4(struct ShaderProgram *shp, const char *param_name, const f32v4 vec);

struct program_uniform_binding {
  const char *name;
  GLenum type;
  GLsizei count;
  const void *value;

  GLint location;
};

void program_mat3_transpose_and_trim(GLfloat *dest, const struct floatmat4 *mat);

void program_prepare_uniform_binding(struct ShaderProgram *shp, struct program_uniform_binding *ub);
void program_prepare_uniform_bindings(struct ShaderProgram *shp, struct program_uniform_binding **ubs);

void program_set_uniform_binding(struct ShaderProgram *shp, struct program_uniform_binding *ub);
void program_set_uniform_bindings(struct ShaderProgram *shp, struct program_uniform_binding **ubs);

#endif
