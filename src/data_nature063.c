
#include "model.h"
#include "texture_asset.h"

#include "data_nature063.inc"

static struct model_buffer_chunk model_nature063_attrib_position_mbc = {
  GL_ARRAY_BUFFER,
  sizeof(GLfloat),
  sizeof(model_nature063_position),
  model_nature063_position,

  0, 0, NULL
};

static struct model_buffer_chunk model_nature063_attrib_texcoord_mbc = {
  GL_ARRAY_BUFFER,
  sizeof(GLfloat),
  sizeof(model_nature063_texcoord),
  model_nature063_texcoord,

  0, 0, NULL
};

static struct model_buffer_chunk model_nature063_attrib_normal_mbc = {
  GL_ARRAY_BUFFER,
  sizeof(GLfloat),
  sizeof(model_nature063_normal),
  model_nature063_normal,

  0, 0, NULL
};

static struct model_buffer_chunk model_nature063_indices_Leafs_mbc = {
  GL_ELEMENT_ARRAY_BUFFER,
  sizeof(GLushort),
  sizeof(model_nature063_indices_Leafs),
  model_nature063_indices_Leafs,

  0, 0, NULL
};

static struct model_buffer_chunk model_nature063_indices_Wood_mbc = {
  GL_ELEMENT_ARRAY_BUFFER,
  sizeof(GLushort),
  sizeof(model_nature063_indices_Wood),
  model_nature063_indices_Wood,

  0, 0, NULL
};

static struct model_buffer_chunk *model_nature063_mbclist[] = {
  &model_nature063_attrib_position_mbc,
  &model_nature063_attrib_texcoord_mbc,
  &model_nature063_attrib_normal_mbc,
  &model_nature063_indices_Leafs_mbc,
  &model_nature063_indices_Wood_mbc,
  NULL
};

static struct model_attrib_array model_nature063_attrib_position = {
  &model_nature063_attrib_position_mbc,
  0,

  MODEL_STDATTR_POSITION,
  3,
  GL_FLOAT,
  GL_FALSE,
  0
};

static struct model_attrib_array model_nature063_attrib_texcoord = {
  &model_nature063_attrib_texcoord_mbc,
  0,

  MODEL_STDATTR_TEXCOORD,
  2,
  GL_FLOAT,
  GL_FALSE,
  0
};

static struct model_attrib_array model_nature063_attrib_normal = {
  &model_nature063_attrib_normal_mbc,
  0,

  MODEL_STDATTR_NORMAL,
  3,
  GL_FLOAT,
  GL_FALSE,
  0
};

static struct model_attrib_array *model_nature063_attrib_array_list[] = {
  &model_nature063_attrib_position,
  &model_nature063_attrib_texcoord,
  &model_nature063_attrib_normal,
  NULL
};

static struct model_drawpart_element_array model_nature063_drawpart_Leafs_indices = {
  &model_nature063_indices_Leafs_mbc,
  0,

  GL_TRIANGLES,
  GL_UNSIGNED_SHORT,
  sizeof(model_nature063_indices_Leafs)/(sizeof(model_nature063_indices_Leafs[0])*3)
};

static GLint model_nature063_drawpart_Leafs_uniform_values_int[] = { 0, 1, 2 };
static struct program_uniform_binding model_nature063_drawpart_Leafs_uniform_mtl_tex_diffamb = {
  "mtl_tex_diffamb",
  GL_SAMPLER_2D,
  1,
  &(model_nature063_drawpart_Leafs_uniform_values_int[0]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Leafs_uniform_mtl_tex_normal = {
  "mtl_tex_normal",
  GL_SAMPLER_2D,
  1,
  &(model_nature063_drawpart_Leafs_uniform_values_int[1]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Leafs_uniform_mtl_tex_specular = {
  "mtl_tex_specular",
  GL_SAMPLER_2D,
  1,
  &(model_nature063_drawpart_Leafs_uniform_values_int[2]),
  -1
};

static GLfloat model_nature063_drawpart_Leafs_uniform_values_float[] = {
  /* mtl_color_ambient */
  0.000000, 0.000000, 0.000000, 1.0,
  /* mtl_color_diffuse */
  0.270588, 0.407843, 0.400000, 1.0,
  /* mtl_color_specular */
  0.330000, 0.330000, 0.330000, 1.0,
  /* mtl_specular_exponent */
  10.0,
};
static struct program_uniform_binding model_nature063_drawpart_Leafs_uniform_mtl_color_ambient = {
  "mtl_color_ambient",
  GL_FLOAT_VEC4,
  1,
  &(model_nature063_drawpart_Leafs_uniform_values_float[0]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Leafs_uniform_mtl_color_diffuse = {
  "mtl_color_diffuse",
  GL_FLOAT_VEC4,
  1,
  &(model_nature063_drawpart_Leafs_uniform_values_float[4]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Leafs_uniform_mtl_color_specular = {
  "mtl_color_specular",
  GL_FLOAT_VEC4,
  1,
  &(model_nature063_drawpart_Leafs_uniform_values_float[8]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Leafs_uniform_mtl_specular_exponent = {
  "mtl_specular_exponent",
  GL_FLOAT,
  1,
  &(model_nature063_drawpart_Leafs_uniform_values_float[12]),
  -1
};

static struct program_uniform_binding *model_nature063_drawpart_Leafs_uniforms[] = {
  &model_nature063_drawpart_Leafs_uniform_mtl_tex_diffamb,
  &model_nature063_drawpart_Leafs_uniform_mtl_tex_normal,
  &model_nature063_drawpart_Leafs_uniform_mtl_tex_specular,
  &model_nature063_drawpart_Leafs_uniform_mtl_color_ambient,
  &model_nature063_drawpart_Leafs_uniform_mtl_color_diffuse,
  &model_nature063_drawpart_Leafs_uniform_mtl_color_specular,
  &model_nature063_drawpart_Leafs_uniform_mtl_specular_exponent,
  NULL
};

static struct texture_binding model_nature063_drawpart_Leafs_tex_diffamb  = { GL_TEXTURE0, NULL };
static struct texture_binding model_nature063_drawpart_Leafs_tex_normal   = { GL_TEXTURE1, NULL };
static struct texture_binding model_nature063_drawpart_Leafs_tex_specular = { GL_TEXTURE2, NULL };
static struct texture_binding *model_nature063_drawpart_Leafs_texture_bindings[] = {
  &model_nature063_drawpart_Leafs_tex_diffamb,
  &model_nature063_drawpart_Leafs_tex_normal,
  &model_nature063_drawpart_Leafs_tex_specular,
  NULL
};

static struct model_drawpart model_nature063_drawpart_Leafs = {
  &model_shader_program_stdmodel,
  model_nature063_drawpart_Leafs_uniforms,
  model_nature063_drawpart_Leafs_texture_bindings,
  &model_nature063_drawpart_Leafs_indices
};

static struct model_drawpart_element_array model_nature063_drawpart_Wood_indices = {
  &model_nature063_indices_Wood_mbc,
  0,

  GL_TRIANGLES,
  GL_UNSIGNED_SHORT,
  sizeof(model_nature063_indices_Wood)/(sizeof(model_nature063_indices_Wood[0])*3)
};

static GLint model_nature063_drawpart_Wood_uniform_values_int[] = { 0, 1, 2 };
static struct program_uniform_binding model_nature063_drawpart_Wood_uniform_mtl_tex_diffamb = {
  "mtl_tex_diffamb",
  GL_SAMPLER_2D,
  1,
  &(model_nature063_drawpart_Wood_uniform_values_int[0]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Wood_uniform_mtl_tex_normal = {
  "mtl_tex_normal",
  GL_SAMPLER_2D,
  1,
  &(model_nature063_drawpart_Wood_uniform_values_int[1]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Wood_uniform_mtl_tex_specular = {
  "mtl_tex_specular",
  GL_SAMPLER_2D,
  1,
  &(model_nature063_drawpart_Wood_uniform_values_int[2]),
  -1
};

static GLfloat model_nature063_drawpart_Wood_uniform_values_float[] = {
  /* mtl_color_ambient */
  0.000000, 0.000000, 0.000000, 1.0,
  /* mtl_color_diffuse */
  0.666667, 0.545098, 0.356863, 1.0,
  /* mtl_color_specular */
  0.330000, 0.330000, 0.330000, 1.0,
  /* mtl_specular_exponent */
  10.0,
};
static struct program_uniform_binding model_nature063_drawpart_Wood_uniform_mtl_color_ambient = {
  "mtl_color_ambient",
  GL_FLOAT_VEC4,
  1,
  &(model_nature063_drawpart_Wood_uniform_values_float[0]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Wood_uniform_mtl_color_diffuse = {
  "mtl_color_diffuse",
  GL_FLOAT_VEC4,
  1,
  &(model_nature063_drawpart_Wood_uniform_values_float[4]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Wood_uniform_mtl_color_specular = {
  "mtl_color_specular",
  GL_FLOAT_VEC4,
  1,
  &(model_nature063_drawpart_Wood_uniform_values_float[8]),
  -1
};
static struct program_uniform_binding model_nature063_drawpart_Wood_uniform_mtl_specular_exponent = {
  "mtl_specular_exponent",
  GL_FLOAT,
  1,
  &(model_nature063_drawpart_Wood_uniform_values_float[12]),
  -1
};

static struct program_uniform_binding *model_nature063_drawpart_Wood_uniforms[] = {
  &model_nature063_drawpart_Wood_uniform_mtl_tex_diffamb,
  &model_nature063_drawpart_Wood_uniform_mtl_tex_normal,
  &model_nature063_drawpart_Wood_uniform_mtl_tex_specular,
  &model_nature063_drawpart_Wood_uniform_mtl_color_ambient,
  &model_nature063_drawpart_Wood_uniform_mtl_color_diffuse,
  &model_nature063_drawpart_Wood_uniform_mtl_color_specular,
  &model_nature063_drawpart_Wood_uniform_mtl_specular_exponent,
  NULL
};

static struct texture_binding model_nature063_drawpart_Wood_tex_diffamb  = { GL_TEXTURE0, NULL };
static struct texture_binding model_nature063_drawpart_Wood_tex_normal   = { GL_TEXTURE1, NULL };
static struct texture_binding model_nature063_drawpart_Wood_tex_specular = { GL_TEXTURE2, NULL };
static struct texture_binding *model_nature063_drawpart_Wood_texture_bindings[] = {
  &model_nature063_drawpart_Wood_tex_diffamb,
  &model_nature063_drawpart_Wood_tex_normal,
  &model_nature063_drawpart_Wood_tex_specular,
  NULL
};

static struct model_drawpart model_nature063_drawpart_Wood = {
  &model_shader_program_stdmodel,
  model_nature063_drawpart_Wood_uniforms,
  model_nature063_drawpart_Wood_texture_bindings,
  &model_nature063_drawpart_Wood_indices
};

static struct model_drawpart *model_nature063_drawparts[] = {
  &model_nature063_drawpart_Leafs,
  &model_nature063_drawpart_Wood,
  NULL
};

struct model model_nature063 = {
  model_nature063_mbclist,
  NULL,
  model_nature063_attrib_array_list,
  model_nature063_drawparts,
  0, 0
};

static void model_nature063_init_textures() {
  model_nature063_drawpart_Leafs_tex_diffamb .tex = load_texture_from_asset("textures/white.webp",      GL_TEXTURE_2D);
  model_nature063_drawpart_Leafs_tex_normal  .tex = load_texture_from_asset("textures/flatnormal.webp", GL_TEXTURE_2D);
  model_nature063_drawpart_Leafs_tex_specular.tex = load_texture_from_asset("textures/white.webp",      GL_TEXTURE_2D);
  model_nature063_drawpart_Wood_tex_diffamb .tex = load_texture_from_asset("textures/white.webp",      GL_TEXTURE_2D);
  model_nature063_drawpart_Wood_tex_normal  .tex = load_texture_from_asset("textures/flatnormal.webp", GL_TEXTURE_2D);
  model_nature063_drawpart_Wood_tex_specular.tex = load_texture_from_asset("textures/white.webp",      GL_TEXTURE_2D);
};

void model_nature063_init() {
  model_nature063_init_textures();
  model_prepare(&model_nature063);
};

