
#include <SDL.h>

#include <string.h>

#include "texture.h"

static int is_emp(const uint8_t *data, size_t size) {
  static const uint8_t magic[] = {'E', 'M', 'P', ' ', '1', '0', 0, 0};
  return (memcmp(data, magic, 8) == 0);
};

static int is_pkm(const uint8_t *data, size_t size) {
  static const uint8_t magic[] = {'P', 'K', 'M', ' ', '1', '0', 0, 0};
  return (memcmp(data, magic, 8) == 0);
};

static int is_webp(const uint8_t *data, size_t size) {
  static const uint8_t magic1[] = "RIFF";
  static const uint8_t magic2[] = "WEBP";
  return (memcmp(data, magic1, 4) == 0) && (memcmp(data+8, magic2, 4) == 0);
};

void set_texture_image_any(struct texture *tex, GLenum target, const uint8_t *data, size_t size) {
  if (is_emp(data, size)) {
    set_texture_image_emp(tex, target, data, size);
  } else if (is_pkm(data, size)) {
    set_texture_image_pkm(tex, target, data, size);
  } else if (is_webp(data, size)) {
    set_texture_image_webp(tex, target, data, size);
  } else {
    SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                "unable to determine format of image for texture %s",
                tex->obj_name);
  };
};

