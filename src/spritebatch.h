#ifndef X1eo6c0msosve4yvs9zbuynpcj957ibu5nazo0qalemolmdmxnmuzgha9nklhfrun
#define X1eo6c0msosve4yvs9zbuynpcj957ibu5nazo0qalemolmdmxnmuzgha9nklhfrun

#include <stddef.h>
#include <stdint.h>

#include <SDL_opengles2.h>

#include "shader.h"
#include "texture.h"

struct SpriteBatchConfig {
  GLuint sprite_capacity;
  GLenum buffer_usage;
  GLuint max_sprite_display_size;
  
  
};


struct SpriteBatchImpl {
  
  
  
};

struct SpriteBatch {
  struct SpriteBatchConfig cfg;

  struct SpriteBatchImpl *impl;

  
  
  
  
};

struct SpriteBatch *spritebatch_new();
void spritebatch_free(struct SpriteBatch *sb);

GLenum spritebatch_get_last_texture_unit_used(struct SpriteBatch *sb);
struct Texture *spritebatch_get_texture_binding(struct SpriteBatch *sb, GLenum texunit);
void spritebatch_set_texture_binding(struct SpriteBatch *sb, GLenum texunit, struct Texture *tex);

struct ShaderProgram *spritebatch_get_shader_program_poly(struct SpriteBatch *sb);
void spritebatch_set_shader_program_poly(struct SpriteBatch *sb, struct ShaderProgram *prog);

unsigned int spritebatch_get_sprite_capacity(struct SpriteBatch *sb);
void spritebatch_set_sprite_capacity(struct SpriteBatch *sb, unsigned int n_sprites);





#endif
