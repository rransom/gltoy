
#include <SDL.h>

#include "shader.h"

#include "db.h"
#include "objmap.h"

struct compile_info_spec {
  GLenum compile_status;
  const char *compile_type;
  GLvoid (*glGet_iv)(GLuint obj, GLenum pname, GLint *param);
  const char *obj_type;
  GLvoid (*glGet_InfoLog)(GLuint obj, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
};

static void dump_compile_info(const struct compile_info_spec *spec,
                              GLuint obj, const char *obj_name) {

  GLint compile_status = GL_FALSE;
  GLchar *info_log_buf = NULL;
  GLint info_log_len = 0;

  spec->glGet_iv(obj, spec->compile_status, &compile_status);
  if (compile_status != GL_TRUE) {
    SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
		"%s of %s %s failed",
                spec->compile_type,
                spec->obj_type,
		obj_name);
  };

  spec->glGet_iv(obj, GL_INFO_LOG_LENGTH, &info_log_len);
  if (info_log_len != 0) {
    info_log_buf = malloc(info_log_len + 1);
    if (info_log_buf == NULL) {
      SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                  "info log for %s %s has len %i; unable to allocate buffer",
                  spec->obj_type,
                  obj_name,
                  info_log_len);
      return;
    };
    spec->glGet_InfoLog(obj, info_log_len, NULL, info_log_buf);
    info_log_buf[info_log_len] = 0;
    if (strlen(info_log_buf) != 0) {
      SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION,
                  "info log for %s %s (len %i, strlen %i): %s",
                  spec->obj_type,
                  obj_name,
                  info_log_len,
                  (int)strlen(info_log_buf),
                  info_log_buf);
    };
    free(info_log_buf);
  };
};

static const struct compile_info_spec shader_info_spec = {
  GL_COMPILE_STATUS,
  "compile",
  glGetShaderiv,
  "shader",
  glGetShaderInfoLog,
};

static const struct compile_info_spec program_info_spec = {
  GL_LINK_STATUS,
  "link",
  glGetProgramiv,
  "program",
  glGetProgramInfoLog,
};

static const struct compile_info_spec validate_program_info_spec = {
  GL_VALIDATE_STATUS,
  "validate",
  glGetProgramiv,
  "program",
  glGetProgramInfoLog,
};

OBJMAP_PRIVATE(Shader, shader,
  "SELECT shid, shname FROM assets.shader WHERE shid = ?1 OR shname = ?2",
  GLuint globj;
);

OBJMAP_PRIVATE(ShaderProgram, shader_program,
  "SELECT shpid, shpname FROM assets.shaderprogram WHERE shpid = ?1 OR shpname = ?2",
  GLuint globj;
);

DB_STATIC_QUERY_STMT(shader_load_shader,
  "SELECT shtype, bdata FROM assets.shader "
  "    JOIN assets.datablob USING (bid) "
  "  WHERE shid = ?1"
);

static int ensure_shader_loaded(void *obj_) {
  struct Shader *obj = obj_;
  DBSQ_CLIENT_VARDEFS;

  if (obj->globj != 0) return 0;

  DBSQ_START(shader_load_shader);

  DBSQ_BIND_INT64(1, obj->base.dbid);
  DBSQ_CHECK_FOR_BIND_FAILURE;

  if (DBSQ_STEP) {
    int shtype = sqlite3_column_int(shader_load_shader.stmt, 0);
    const char *bdata = sqlite3_column_text(shader_load_shader.stmt, 1);

    obj->globj = glCreateShader(shtype);

    glShaderSource(obj->globj, 1, &bdata, NULL);

    glCompileShader(obj->globj);
    dump_compile_info(&shader_info_spec, obj->globj, obj->base.dbname);
  } else {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                 "no rows found in database for shader %Ld (%s)",
                 (long long int)obj->base.dbid, obj->base.dbname);
    goto err;
  };

  DBSQ_DONE;
  return 0;

 err:
  shader_cleanup(obj);
  DBSQ_CLEANUP_AFTER_ERR;

  SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
               "failed to load shader %Ld (%s)",
               (long long int)obj->base.dbid, obj->base.dbname);

  return -1;
};

static void shader_cleanup(struct Shader *obj) {
  if (obj->globj != 0) {
    glDeleteShader(obj->globj);
    obj->globj = 0;
  };
};

DB_STATIC_QUERY_STMT(shader_program_load_shaders,
  "SELECT shid FROM assets.shaderattachment "
  "  WHERE shpid = ?1"
);

DB_STATIC_QUERY_STMT(shader_program_load_attbinds,
  "SELECT shattname, shattindex FROM assets.shaderattribbindingsetuse "
  "    JOIN assets.shaderattribbinding USING (shabsid) "
  "  WHERE shpid = ?1"
);

static int ensure_shader_program_loaded(void *obj_) {
  struct ShaderProgram *obj = obj_;
  DBSQ_CLIENT_VARDEFS;

  if (obj->globj != 0) return 0;

  obj->globj = glCreateProgram();
  if (obj->globj == 0) goto err;

  DBSQ_START(shader_program_load_shaders);

  DBSQ_BIND_INT64(1, obj->base.dbid);
  DBSQ_CHECK_FOR_BIND_FAILURE;

  while (DBSQ_STEP) {
    int64_t shid = sqlite3_column_int64(shader_program_load_shaders.stmt, 0);
    struct Shader *shader = load_shader_by_dbid(shid);

    if ((shader == NULL) || (shader->globj == 0)) goto err;

    glAttachShader(obj->globj, shader->globj);
  };

  DBSQ_DONE;

  DBSQ_START(shader_program_load_attbinds);

  DBSQ_BIND_INT64(1, obj->base.dbid);
  DBSQ_CHECK_FOR_BIND_FAILURE;

  while (DBSQ_STEP) {
    const char *shattname = sqlite3_column_text(shader_program_load_attbinds.stmt, 0);
    int shattindex = sqlite3_column_int(shader_program_load_attbinds.stmt, 1);

    glBindAttribLocation(obj->globj, shattindex, shattname);
  };

  DBSQ_DONE;

  glLinkProgram(obj->globj);
  dump_compile_info(&program_info_spec, obj->globj, obj->base.dbname);

  /* FIXME run glValidateProgram only during development */               
  glValidateProgram(obj->globj);
  dump_compile_info(&validate_program_info_spec, obj->globj, obj->base.dbname);

  return 0;

 err:
  shader_program_cleanup(obj);
  DBSQ_CLEANUP_AFTER_ERR;

  SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
               "failed to load shader program %Ld (%s)",
               (long long int)obj->base.dbid, obj->base.dbname);

  return -1;
};

static void shader_program_cleanup(struct ShaderProgram *obj) {
  if (obj->globj != 0) {
    glDeleteProgram(obj->globj);
    obj->globj = 0;
  };
};

void use_program(struct ShaderProgram *shp) {
  ensure_shader_program_loaded(shp);
  glUseProgram(shp->globj);
};

static GLuint current_program() {
  GLint rv = 0;
  glGetIntegerv(GL_CURRENT_PROGRAM, &rv);
  return (GLuint)rv;
};

void program_set_uniform_texunit(struct ShaderProgram *shp, const char *param_name, GLenum texunit) {
  GLuint oldprog = current_program();
  use_program(shp);
  glUniform1i(glGetUniformLocation(shp->globj, param_name), texunit - GL_TEXTURE0);
  glUseProgram(oldprog);
};

void program_set_uniform_float(struct ShaderProgram *shp, const char *param_name, GLfloat value) {
  GLuint oldprog = current_program();
  use_program(shp);
  glUniform1f(glGetUniformLocation(shp->globj, param_name), value);
  glUseProgram(oldprog);
};

void program_set_uniform_mat4(struct ShaderProgram *shp, const char *param_name, const struct floatmat4 *mat) {
  GLuint oldprog = current_program();
  use_program(shp);
  glUniformMatrix4fv(glGetUniformLocation(shp->globj, param_name), 1, GL_FALSE, (const GLfloat *)mat);
  glUseProgram(oldprog);
};

void program_set_uniform_mat3_transposed(struct ShaderProgram *shp, const char *param_name, const struct floatmat4 *mat) {
  GLfloat tmp[9];
  GLuint oldprog = current_program();
  use_program(shp);
  program_mat3_transpose_and_trim(tmp, mat);
  glUniformMatrix3fv(glGetUniformLocation(shp->globj, param_name), 1, GL_FALSE, (const GLfloat *)tmp);
  glUseProgram(oldprog);
};

void program_set_uniform_vec4(struct ShaderProgram *shp, const char *param_name, const f32v4 vec) {
  GLuint oldprog = current_program();
  use_program(shp);
  glUniform4fv(glGetUniformLocation(shp->globj, param_name), 1, (const GLfloat *)&vec);
  glUseProgram(oldprog);
};

void program_set_uniform_vec3(struct ShaderProgram *shp, const char *param_name, const f32v4 vec) {
  GLuint oldprog = current_program();
  use_program(shp);
  glUniform3fv(glGetUniformLocation(shp->globj, param_name), 1, (const GLfloat *)&vec);
  glUseProgram(oldprog);
};

void program_set_uniform_vec2(struct ShaderProgram *shp, const char *param_name, const f32v4 vec) {
  GLuint oldprog = current_program();
  use_program(shp);
  glUniform2fv(glGetUniformLocation(shp->globj, param_name), 1, (const GLfloat *)&vec);
  glUseProgram(oldprog);
};

void program_set_attrib_const_floatvec4(struct ShaderProgram *shp, const char *param_name, const f32v4 vec) {
  GLuint oldprog = current_program();
  GLint loc;
  use_program(shp);

  loc = glGetAttribLocation(shp->globj, param_name);
  glDisableVertexAttribArray(loc);
  glVertexAttrib4fv(loc, (const GLfloat *)&vec);

  glUseProgram(oldprog);
};

void program_mat3_transpose_and_trim(GLfloat *dest, const struct floatmat4 *mat) {
  int i, j;
  for (i = 0; i < 3; ++i) {
    for (j = 0; j < 3; ++j) {
      dest[j*3 + i] = mat->x[i][j];
    };
  };
};

void program_prepare_uniform_binding(struct ShaderProgram *shp, struct program_uniform_binding *ub) {
  ensure_shader_program_loaded(shp);
  ub->location = glGetUniformLocation(shp->globj, ub->name);
};

void program_prepare_uniform_bindings(struct ShaderProgram *shp, struct program_uniform_binding **ubs) {
  if (ubs == NULL) return;
  while (*ubs != NULL) {
    program_prepare_uniform_binding(shp, *ubs);
    ++ubs;
  };
};

typedef void (*uniform_set_command_proc)(GLint location, GLsizei count, const void *value);
struct uniform_set_command_rec {
  GLenum type;
  uniform_set_command_proc command;
};

static void uniform_set_command_float_mat2(GLint location, GLsizei count, const void *value) {
  glUniformMatrix2fv(location, count, GL_FALSE, value);
};
static void uniform_set_command_float_mat3(GLint location, GLsizei count, const void *value) {
  glUniformMatrix3fv(location, count, GL_FALSE, value);
};
static void uniform_set_command_float_mat4(GLint location, GLsizei count, const void *value) {
  glUniformMatrix4fv(location, count, GL_FALSE, value);
};

static const struct uniform_set_command_rec uniform_set_commands[] = {
  {GL_FLOAT     ,   (uniform_set_command_proc) glUniform1fv},
  {GL_FLOAT_VEC2,   (uniform_set_command_proc) glUniform2fv},
  {GL_FLOAT_VEC3,   (uniform_set_command_proc) glUniform3fv},
  {GL_FLOAT_VEC4,   (uniform_set_command_proc) glUniform4fv},
  {GL_INT     ,     (uniform_set_command_proc) glUniform1iv},
  {GL_INT_VEC2,     (uniform_set_command_proc) glUniform2iv},
  {GL_INT_VEC3,     (uniform_set_command_proc) glUniform3iv},
  {GL_INT_VEC4,     (uniform_set_command_proc) glUniform4iv},
  {GL_SAMPLER_2D  , (uniform_set_command_proc) glUniform1iv},
  {GL_SAMPLER_CUBE, (uniform_set_command_proc) glUniform1iv},
  {GL_FLOAT_MAT2,   uniform_set_command_float_mat2},
  {GL_FLOAT_MAT3,   uniform_set_command_float_mat3},
  {GL_FLOAT_MAT4,   uniform_set_command_float_mat4},
  {0, NULL}
};

static void program_set_uniform_binding_internal(struct program_uniform_binding *ub) {
  int i;

  for (i = 0; uniform_set_commands[i].type != 0; ++i) {
    if (uniform_set_commands[i].type == ub->type) {
      uniform_set_commands[i].command(ub->location, ub->count, ub->value);
      return;
    };
  };
};

void program_set_uniform_binding(struct ShaderProgram *shp, struct program_uniform_binding *ub) {
  GLuint oldprog = current_program();
  use_program(shp);

  program_set_uniform_binding_internal(ub);

  glUseProgram(oldprog);
};

void program_set_uniform_bindings(struct ShaderProgram *shp, struct program_uniform_binding **ubs) {
  GLuint oldprog = current_program();
  use_program(shp);

  if (ubs == NULL) return;
  while (*ubs != NULL) {
    program_set_uniform_binding_internal(*ubs);
    ++ubs;
  };

  glUseProgram(oldprog);
};

