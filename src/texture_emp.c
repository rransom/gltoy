
#include <SDL.h>

#include "texture.h"

static uint16_t getui16be(const uint8_t *data) {
  return (((uint16_t)data[0]) << 8) + (uint16_t)data[1];
};

#define GL_COMPRESSED_RGB8_ETC2           0x9274

struct pkmheader_data {
  /* stored explicitly and non-constant */
  uint16_t padw;
  uint16_t padh;
  uint16_t imgw;
  uint16_t imgh;
  /* derived */
  size_t imgbytes;
};

static int parse_pkm_header(struct pkmheader_data *out, const uint8_t *data, size_t len) {
  static const uint8_t pkm_magic[] = {'P', 'K', 'M', ' ', '1', '0', 0, 0};

  memset(out, 0, sizeof(*out));

  /* header structure for each PKM mipmap level:
       "PKM 10", 0, 0 (fixed 8-byte magic number)
       width  of padded image, 2-byte big-endian
       height of padded image, 2-byte big-endian
       width  of        image, 2-byte big-endian
       height of        image, 2-byte big-endian
  */

  if (len < 16) return -1;
  if (memcmp(data, pkm_magic, 8) != 0) return -2;

  out->padw = getui16be(data+ 8);
  out->padh = getui16be(data+10);
  out->imgw = getui16be(data+12);
  out->imgh = getui16be(data+14);

  if ((out->padw & 3) != 0) return -3;
  if ((out->padh & 3) != 0) return -4;
  if (out->padw < out->imgw) return -5;
  if (out->padw > out->imgw + 3) return -6;
  if (out->padh < out->imgh) return -7;
  if (out->padh > out->imgh + 3) return -8;

  out->imgbytes = (out->padw >> 1) * out->padh;
  if (out->imgbytes > len - 16) return -9;

  return 0;
};

void set_texture_image_emp(struct texture *tex, GLenum target, const uint8_t *data, size_t size) {
  static const uint8_t emp_magic[8] = {'E', 'M', 'P', ' ', '1', '0', 0, 0};

  struct pkmheader_data head;
  GLint level;
  int headparse_rv;

  if (size < 8) {
    SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                "EMP texture file too small (%d bytes)", (int)size);
    return;
  };

  if (memcmp(data, emp_magic, 8) != 0) {
    SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                "EMP texture file has bad magic");
    return;
  };

  data += 8; size -= 8;
  level = 0;
  while (size > 0) {
    headparse_rv = parse_pkm_header(&head, data, size);
    if (headparse_rv < 0) {
      SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                  "parse failure in EMP texture file (level %d, error %d)",
                  level, -headparse_rv);
      return;
    };

    glCompressedTexImage2D(target, level, GL_ETC1_RGB8_OES, head.imgw, head.imgh, 0, head.imgbytes, data+16);
    /* FIXME load as ETC2 if ES 3.0 is available and the ETC1 extension isn't */     

    data += 16 + head.imgbytes;
    size -= 16 + head.imgbytes;
    ++level;
  };
};

