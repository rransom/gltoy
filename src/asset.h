#ifndef Xleguigm81nie8ts930gdbep7rs9ohjr3i010pwaebewwrbpeo88g3mgwgpw658nw
#define Xleguigm81nie8ts930gdbep7rs9ohjr3i010pwaebewwrbpeo88g3mgwgpw658nw

#include <stddef.h>
#include <stdint.h>

#include <SDL.h>

int asset_init(const char *appname_);
void asset_quit();

SDL_RWops *asset_open(const char *assetname);

uint8_t *asset_load_blob(const char *assetname, size_t *len_out);

#endif
