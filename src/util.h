#ifndef X5qgp2ibqjlmo9k1gfjlndu4vmo3ifa0ef4ldtjmo8320lliry4ggubx7ia9n3trr
#define X5qgp2ibqjlmo9k1gfjlndu4vmo3ifa0ef4ldtjmo8320lliry4ggubx7ia9n3trr

void util_log_sdl_error(const char *ctx);

char *util_strdupcat(const char *x, const char *y);

int util_is_rel_path_safe(const char *x);
int util_test_is_rel_path_safe(); /* unit test; currently run on startup */

void util_log_gl_error_state(const char *ctx);

#endif
