#ifndef X6lf6c5rpidli3gl7vixaee19yvnkajbevviojktd2jvc76c91pbk0qqpbcvkokap
#define X6lf6c5rpidli3gl7vixaee19yvnkajbevviojktd2jvc76c91pbk0qqpbcvkokap

#include "texture.h"

void texture_part_asset_loader(const struct texture_part *part, struct texture *tex);

struct texture *load_texture_from_asset(const char *name, GLenum textarget);

#endif
