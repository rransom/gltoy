
#include <SDL.h>

#include <unistd.h>

#include "asset.h"
#include "util.h"

static int initialized = 0;
static const char *appname = NULL;

struct search_path_entry {
  char *directory;
  /* TODO support non-directory path entries, e.g. zip files, too */
};

/* TODO? allow variable-length search path? */
static struct search_path_entry search_path[5] = {
  { NULL, },
  { NULL, },
  { NULL, },
  { NULL, },
  { NULL, },
};

static void dump_search_path() {
  int i;

  for (i = 0; search_path[i].directory != NULL; ++i) {
    SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION,
		"asset search path item %i: %s",
		i, search_path[i].directory);
  };
};

int asset_init(const char *appname_) {
  int i;

  if (initialized) return 0;
  if (appname_ == NULL) return 0;

  if (util_test_is_rel_path_safe() != 0) {
    return -1;
  };

  initialized = 1;
  appname = appname_;

  for (i = 0; search_path[i].directory != NULL; ++i) {
    SDL_free(search_path[i].directory);
    search_path[i].directory = NULL;
  };

  i = 0;

#if SDL_VERSION_ATLEAST(2, 0, 1)
  {
    char *base_path = SDL_GetBasePath();
    if (base_path != NULL) {
      size_t base_path_len = SDL_strlen(base_path);
      char path_sep = base_path[base_path_len - 1];

      search_path[i].directory = util_strdupcat(base_path, "dat/");
      ++i;

      base_path[base_path_len - 1] = 0;
      (*(SDL_strrchr(base_path, path_sep) + 1)) = 0;
      search_path[i].directory = util_strdupcat(base_path, "dat/");
      ++i;

      SDL_free(base_path);
    };
  };
#endif

  {
    char cwdbuf[4097];
    getcwd(cwdbuf, 4096);
    cwdbuf[4096] = 0;

    search_path[i].directory = util_strdupcat(cwdbuf, "/dat/");
    ++i;
  };

  if (SDL_getenv("HOME") != NULL) {
    char *tmp1 = util_strdupcat(SDL_getenv("HOME"), "/.local/share/");
    char *tmp2 = util_strdupcat(tmp1, appname);

    search_path[i].directory = util_strdupcat(tmp2, "/dat/");
    ++i;

    SDL_free(tmp1);
    SDL_free(tmp2);
  };

  dump_search_path();

  return 0;
};

void asset_quit() {
  int i;

  if (initialized) {
    for (i = 0; search_path[i].directory != NULL; ++i) {
      SDL_free(search_path[i].directory);
      search_path[i].directory = NULL;
    };

    initialized = 0;
  };
};

SDL_RWops *asset_open(const char *assetname) {
  int i;
  char *filename = NULL;
  SDL_RWops *rv = NULL;

  if (!util_is_rel_path_safe(assetname)) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "asset name %s is unsafe",
		 assetname);
    return NULL;
  };

  for (i = 0; (search_path[i].directory != NULL) && (rv == NULL); ++i) {
    filename = util_strdupcat(search_path[i].directory, assetname);
    rv = SDL_RWFromFile(filename, "rb");

    SDL_free(filename);
    filename = NULL;
  };

  if (rv == NULL) {
    /* util_log_sdl_error("SDL_RWFromFile"); /* useless at this point */
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "error opening asset %s",
		 assetname);
  };

  return rv;
};

uint8_t *asset_load_blob(const char *assetname, size_t *len_out) {
  SDL_RWops *rw = asset_open(assetname);
  Sint64 size_64;
  size_t size;
  uint8_t *rv = NULL;

  if (rw == NULL) {
    size = 0;
    goto err;
  };

  size_64 = SDL_RWsize(rw);
  size = size_64;

  if (size_64 == -1) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "size of asset %s is unknown",
		 assetname);
    size = 0;
    goto err;
  };

  SDL_assert(size == size_64);

  rv = SDL_malloc(size + 1);
  if (rv == NULL) {
    goto err;
  };

  rv[size] = 0;

  {
    size_t pos = 0;
    while (pos < size) {
      size_t n_read = SDL_RWread(rw, rv + pos, 1, size - pos);

      SDL_assert(n_read <= (size - pos));

      if (n_read == 0) {
	/* shouldn't be EOF because pos < size */
	util_log_sdl_error("SDL_RWread");
	goto err;
      };

      pos += n_read;
    };
  };

 end:
  if (len_out == NULL) {
    SDL_assert(strlen(rv) == size);
  } else {
    *len_out = size;
  };

  return rv;

 err:
  if (rv != NULL) {
    SDL_free(rv);
    rv = NULL;
  };
  goto end;
};

