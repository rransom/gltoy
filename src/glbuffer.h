#ifndef Xlaf67xlr3gwb612j222l47hlfcslnprmaxhxu26anlji3exia2j2va084dfjjkc7
#define Xlaf67xlr3gwb612j222l47hlfcslnprmaxhxu26anlji3exia2j2va084dfjjkc7

#include <stddef.h>
#include <stdint.h>

#include <SDL_opengles2.h>

struct GLBufferSegment;
struct GLBuffer;
struct GLBufferSet;

struct GLBufferSegment {
  GLenum buffer_target;
  GLenum buffer_usage;
  GLboolean segment_size_is_dynamic;
  GLuint align;

  GLsizeiptr size;
  const void *client_pointer;

  struct GLBuffer *buffer;
  const void *buffer_pointer;
  GLsizeiptr size_in_buffer;
  struct GLBufferSegment *next_segment_in_buffer;
};

struct GLBuffer {
  GLenum target;
  GLenum usage;
  GLboolean buffer_is_single_segment;
  GLuint buffer;

  struct GLBufferSegment *first_segment;
  struct GLBufferSet *buffer_set;
  struct GLBuffer *next_buffer;
};

struct GLBufferSet {
  struct GLBuffer *first_shared_buffer;
  struct GLBuffer *first_dedicated_buffer;
};

void glbuffer_set_delete(struct GLBufferSet *bset);

void glbuffer_segment_add_to_set(struct GLBufferSet *bset, struct GLBufferSegment *bseg);
void glbuffer_segment_delete_from_set(struct GLBufferSegment *bseg);

void glbuffer_segment_update(struct GLBufferSegment *bseg);

#endif
