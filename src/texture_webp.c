
#include <SDL.h>

#include <webp/decode.h>
#include <stdlib.h>

#include "texture.h"

static int webp_loader_func(struct TextureLoaderArgs *args) {
  int width = 0;
  int height = 0;
  uint8_t *rgba_data = WebPDecodeRGBA(args->data, args->data_len, &width, &height);

  if (rgba_data == NULL) {
    return -1;
  };

  if (args->tex_width_out != NULL) (*(args->tex_width_out)) = width;
  if (args->tex_height_out != NULL) (*(args->tex_height_out)) = height;

  glTexImage2D(args->image_target->teximage_target, args->level, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, rgba_data);

  free(rgba_data);
  return 0;
};

void register_texture_loader_webp() {
  register_texture_image_loader_func("webp", webp_loader_func, 0);
};

