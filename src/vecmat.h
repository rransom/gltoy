#ifndef X2vy5tabe8139fiygmc2b0sq6zmvsomhjm8kcmpeo4byg4ilwcbbi3iratytszgd5
#define X2vy5tabe8139fiygmc2b0sq6zmvsomhjm8kcmpeo4byg4ilwcbbi3iratytszgd5

#include <stddef.h>
#include <stdint.h>

/* NOTE: uses GCC extensions */

typedef int32_t si32v4 __attribute__ ((vector_size (16)));
typedef float f32v4 __attribute__ ((vector_size (16)));

struct fixedmat4 {
  si32v4 x[4];
};

struct floatmat4 {
  f32v4 x[4];
};

enum vecaxis {
  AXIS_X = 0,
  AXIS_Y = 1,
  AXIS_Z = 2,
  AXIS_W = 3,
};

void mat_float_identity(struct floatmat4 *out);

/* projection matrix functions from GL 1.x; expected to be called infrequently */
void mat_proj_frustum(struct floatmat4 *out, struct floatmat4 *outinv, float l, float r, float b, float t, float n, float f);
void mat_proj_ortho(struct floatmat4 *out, struct floatmat4 *outinv, float l, float r, float b, float t, float n, float f);

/* projection matrix function from GLU, with FOV in caller-selected axis */
void mat_proj_perspective(struct floatmat4 *out, struct floatmat4 *outinv, int fovaxis, float fovdeg, float aspect, float near, float far);

/* also negates Z axis, to display RH coordinate system using LH GLES */
void mat_proj_perspective_rh(struct floatmat4 *out, struct floatmat4 *outinv, int fovaxis, float fovdeg, float aspect, float near, float far);

void mat_float_translate(struct floatmat4 *out, struct floatmat4 *outinv, f32v4 offset);

f32v4 quat_mult_float(f32v4 x, f32v4 y);
f32v4 quat_rescale(f32v4 x);
float quat_magsq_float(f32v4 quat);

f32v4 quat_rotate_vector_float(f32v4 quat, f32v4 vec);

f32v4 quat_frame_float(f32v4 dir, f32v4 up);

f32v4 vec_normalize(f32v4 x);

void mat_rot_from_quat_float(struct floatmat4 *out, struct floatmat4 *outinv, f32v4 in);
void mat_from_quat_pos_float(struct floatmat4 *out, struct floatmat4 *outinv, f32v4 rotquat, f32v4 pos);

void mat_mult_float(struct floatmat4 *out, const struct floatmat4 *a, const struct floatmat4 *b);
void mat_copy_float(struct floatmat4 *out, const struct floatmat4 *in);

#endif
