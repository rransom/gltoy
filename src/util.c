
#include <SDL.h>

void util_log_sdl_error(const char *ctx) {
  SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s: %s", ctx, SDL_GetError());
  SDL_ClearError();
};

char *util_strdupcat(const char *x, const char *y) {
  size_t lenx = SDL_strlen(x);
  size_t leny = SDL_strlen(y);
  char *rv = SDL_malloc(lenx + leny + 1);

  if (rv == NULL) {
    goto end;
  };

  SDL_memcpy(rv,        x, lenx);
  SDL_memcpy(rv + lenx, y, leny);
  rv[lenx + leny] = 0;

 end:
  return rv;
};

int util_is_rel_path_safe(const char *x) {
  size_t lenx = SDL_strlen(x);
  size_t i;
  static const char valid_path_element_chars[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789-_";

  if (lenx == 0) {
    return 0;
  };

  if ((x[0] == '/') || (x[0] == '.')) {
    return 0;
  };

  for (i = 0; i < lenx; ++i) {
    if (x[i] == '/') {
      if (x[i-1] == '/') {
	return 0;
      };
      continue;
    };

    if (x[i] == '.') {
      if ((x[i-1] == '.') || (x[i-1] == '/')) {
	return 0;
      };
      continue;
    };

    if (SDL_strchr(valid_path_element_chars, x[i]) == NULL) {
      return 0;
    };
  };

  return 1;
};

struct is_rel_path_safe_test_entry {
  const char *path;
  int is_safe;
};
static const struct is_rel_path_safe_test_entry is_rel_path_safe_test_array[] =
  {
    { "/foo", 0 },
    { ".foo", 0 },
    { "/.foo", 0 },
    { "../foo", 0 },
    { "foo/..", 0 },
    { "foo/../bar", 0 },
    { "foo..bar", 0 },
    { "foo.bar", 1 },
    { "fred/foo.bar", 1 },
    { "fred/foo.bar/", 1 },
    { NULL, -1 },
  };

int util_test_is_rel_path_safe() {
  int i;

  for (i = 0; is_rel_path_safe_test_array[i].path != NULL; ++i) {
    const struct is_rel_path_safe_test_entry *pentry =
      &(is_rel_path_safe_test_array[i]);

    if (util_is_rel_path_safe(pentry->path) != pentry->is_safe) {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
		   "util_test_is_rel_path_safe: path %s failed test "
		   "(expected result: %i)",
		   pentry->path, pentry->is_safe);
      return -1;
    };
  };

  return 0;
};

