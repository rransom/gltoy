
#include <SDL.h>

#include "texture.h"

static uint16_t getui16be(const uint8_t *data) {
  return (((uint16_t)data[0]) << 8) + (uint16_t)data[1];
};

#define GL_COMPRESSED_RGB8_ETC2           0x9274

void set_texture_image_pkm(struct texture *tex, GLenum target, const uint8_t *data, size_t size) {
  /* FIXME? load as ETC2 if ES 3.0 is available and the ETC1 extension isn't? */     

  /* header structure:
       "PKM 10", 0, 0 (fixed 8-byte magic number)
       width  of padded image, 2-byte big-endian
       height of padded image, 2-byte big-endian
       width  of        image, 2-byte big-endian
       height of        image, 2-byte big-endian
  */

  static const uint8_t magic[] = {'P', 'K', 'M', ' ', '1', '0', 0, 0};
  size_t width, height, padwidth, padheight;

  SDL_assert(size >= 16);
  if (size < 16) return;

  SDL_assert(memcmp(data, magic, 8) == 0);

     width  = getui16be(data+12);
     height = getui16be(data+14);
  padwidth  = getui16be(data+ 8);
  padheight = getui16be(data+10);

  SDL_assert((padwidth  & 3) == 0);
  SDL_assert((padheight & 3) == 0);
  SDL_assert((width  <= padwidth ) && (padwidth  <= width  + 3));
  SDL_assert((height <= padheight) && (padheight <= height + 3));
  SDL_assert(((padwidth >> 1) * padheight) == (size - 16));

  glCompressedTexImage2D(target, 0, GL_ETC1_RGB8_OES, width, height, 0, size-16, data+16);
  //glCompressedTexImage2D(target, 0, GL_COMPRESSED_RGB8_ETC2, width, height, 0, size-16, data+16);

  /* FIXME add a more general param-setting mechanism */               
  glTexParameteri(tex->target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(tex->target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(tex->target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(tex->target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
};

