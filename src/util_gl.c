
#include <SDL.h>
#include <SDL_opengles2.h>

#include "util.h"

void util_log_gl_error_state(const char *ctx) {
  int invalid_enum = 0;
  int invalid_fb_op = 0;
  int invalid_value = 0;
  int invalid_op = 0;
  int oom = 0;
  int frog = 0;
  int any_err = 0;
  GLenum err;

  do {
    err = glGetError();
    switch (err) {
    case GL_NO_ERROR:
      break;
    case GL_INVALID_ENUM:
      any_err = invalid_enum = 1;
      break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:
      any_err = invalid_fb_op = 1;
      break;
    case GL_INVALID_VALUE:
      any_err = invalid_value = 1;
      break;
    case GL_INVALID_OPERATION:
      any_err = invalid_op = 1;
      break;
    case GL_OUT_OF_MEMORY:
      any_err = oom = 1;
      break;
    default:
      any_err = frog = 1;
      break;
    };
  } while (err != GL_NO_ERROR);

  if (any_err) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                 "%s: GL error (enum=%d, fb_op=%d, value=%d, op=%d, oom=%d, frog=%d)",
                 ctx,
                 invalid_enum,
                 invalid_fb_op,
                 invalid_value,
                 invalid_op,
                 oom,
                 frog);
  };
};

