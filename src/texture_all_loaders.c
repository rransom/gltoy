
#include "texture.h"

void register_all_texture_loaders() {
  register_texture_loader_webp();
  register_texture_loader_setminmagfilters();
};

