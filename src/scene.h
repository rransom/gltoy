#ifndef Xakg6pyrxr4pn0pwmvhotqxg580ck81uu7oqeg06m2qz1dtctf684p93w551zp0h6
#define Xakg6pyrxr4pn0pwmvhotqxg580ck81uu7oqeg06m2qz1dtctf684p93w551zp0h6

struct scene_viewport {
  GLint x, y;
  GLsizei w, h;
};

struct scene_state {
  struct scene_viewport viewport;
};

struct scene_impl {
  void (*init)(struct scene_state *);
  int (*update)(struct scene_state *);
  void (*draw)(struct scene_state *);
  void (*cleanup)(struct scene_state *);
};

#endif
