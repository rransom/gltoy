#ifndef Xk87adowe9msksiaoe6hlsbob5ex2o8fabpo2dl7o9zqqaa2d0p1g111d4jw092nz
#define Xk87adowe9msksiaoe6hlsbob5ex2o8fabpo2dl7o9zqqaa2d0p1g111d4jw092nz

#include <stddef.h>
#include <stdint.h>

#include <SDL_opengles2.h>

#include "shader.h"
#include "texture.h"

struct model_buffer_chunk;
struct model_attrib_const;
struct model_attrib_array;
struct model_drawpart_uniform;
struct model_drawpart_element_array;
struct model_drawpart;
struct model;

struct model_buffer_chunk {
  GLenum buffer_target;
  GLuint align;
  GLsizeiptr size;
  const void *client_pointer;

  int is_in_buffer;
  GLuint buffer;
  const void *buffer_pointer;
};

void model_buffer_chunk_prepare(struct model_buffer_chunk **chunks);

struct model_attrib_const {
  GLuint index;
  GLfloat value[4];
};

struct model_attrib_array {
  struct model_buffer_chunk *data;
  GLsizei offset;

  GLuint index;
  GLint attrib_value_size;
  GLenum type;
  GLboolean normalized;
  GLsizei stride;
};

struct model_drawpart_element_array {
  struct model_buffer_chunk *data;
  GLsizei offset;

  GLenum mode;
  GLenum type;
  GLsizei count;
};

struct model_drawpart {
  struct program_spec *program;
  struct program_uniform_binding **prog_uniforms;
  struct texture_binding **textures;
  struct model_drawpart_element_array *element_array;
};

void model_drawpart_prepare(struct model_drawpart *dp);

struct model {
  struct model_buffer_chunk **buffer_chunks;
  struct model_attrib_const **attrib_consts;
  struct model_attrib_array **attrib_arrays;
  struct model_drawpart **drawparts;
  int is_in_vao;
  GLuint vao;
};

void model_prepare(struct model *model);

void model_prepare_parameters(struct model *model, struct program_uniform_binding **uniforms);

void model_draw(struct model *model, struct program_uniform_binding **uniforms);

#define MODEL_STDATTR_POSITION  0
#define MODEL_STDATTR_TEXCOORD  1
#define MODEL_STDATTR_NORMAL    2
#define MODEL_STDATTR_TANGENT_S 3
#define MODEL_STDATTR_TANGENT_T 4
extern const struct attribute_binding *model_stdattbinds[];

extern struct program_spec model_shader_program_stdmodel;

#endif
