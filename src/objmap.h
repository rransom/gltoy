#ifndef X1t3xjkq26ud9emumhhf5grrx0kshuixt1kqtbjr8n2wzsxr9u29zxxpxzzbze8kf
#define X1t3xjkq26ud9emumhhf5grrx0kshuixt1kqtbjr8n2wzsxr9u29zxxpxzzbze8kf

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#include "tree234.h"

#include "db.h"

struct objmap_entry_head {
  int64_t dbid;
  char *dbname;
};

typedef void *(*objmap_entry_allocator)();
typedef void (*objmap_entry_destructor)(void *);
typedef int (*objmap_entry_loader)(void *);

struct objmap {
  tree234 *byid;
  tree234 *byname;
  const char *structname;
  /* idquery must have parameters (id, name) and result columns (id, name) */
  struct db_static_query *idquery;
  objmap_entry_allocator alloc;
  objmap_entry_destructor dtor;
  objmap_entry_loader loader;
};

#define OBJMAP_DEF(structname, funcnamepart, idquery, fields)           \
  struct structname {                                                   \
    struct objmap_entry_head base;                                      \
    fields                                                              \
  };                                                                    \
  struct structname *load_##funcnamepart##_by_name(const char *dbname); \
  struct structname *load_##funcnamepart##_by_dbid(int64_t dbid)        \

#define OBJMAP_IMPL(structname, funcnamepart, idquery, fields)          \
  DB_STATIC_QUERY_STMT(structname##_idquery, idquery);                  \
  static int ensure_##funcnamepart##_loaded(void *obj_);                \
  static void *alloc_##structname() {                                   \
    return calloc(1, sizeof(struct structname));                        \
  };                                                                    \
  static void funcnamepart##_cleanup(struct structname *obj);           \
  static void funcnamepart##_free(void *obj_) {                         \
    if (obj_ == NULL) return;                                           \
    funcnamepart##_cleanup(obj_);                                       \
    free(obj_);                                                         \
  };                                                                    \
  static struct objmap structname##_objmap = { NULL, NULL, #structname, &structname##_idquery, alloc_##structname, funcnamepart##_free, ensure_##funcnamepart##_loaded }; \
  struct structname *load_##funcnamepart##_by_name(const char *dbname) { \
    return objmap_load_by_name(&structname##_objmap, dbname);           \
  };                                                                    \
  struct structname *load_##funcnamepart##_by_dbid(int64_t dbid) {      \
    return objmap_load_by_dbid(&structname##_objmap, dbid);             \
  }                                                                     \

#define OBJMAP_PRIVATE(structname, funcnamepart, idquery, fields)       \
  OBJMAP_DEF(structname, funcnamepart, idquery, fields);                \
  OBJMAP_IMPL(structname, funcnamepart, idquery, fields)                \

void *objmap_load_by_name(struct objmap *map, const char *dbname);
void *objmap_load_by_dbid(struct objmap *map, int64_t dbid);

#endif
