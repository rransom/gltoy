#ifndef X2ppk995zwm8m4pl3ijqnofmxqc14wmy6t1zihao3sypc86d9ydlp9pb58fms2v9h
#define X2ppk995zwm8m4pl3ijqnofmxqc14wmy6t1zihao3sypc86d9ydlp9pb58fms2v9h

#include <stddef.h>
#include <stdint.h>

#include <SDL_opengles2.h>

#include "objmap.h"

struct TextureImageTarget {
  const char *name;
  GLenum texture_bind_target;
  GLenum teximage_target;
};

struct TextureTarget {
  const char *name;
  size_t n_image_targets;
  const struct TextureImageTarget *image_targets;
  GLenum bind_target;
  GLenum binding_query_parameter;
};

int check_texture_targets_arrays();
const struct TextureTarget *lookup_texture_target_by_name(const char *name);
const struct TextureImageTarget *lookup_texture_image_target_by_name(const char *name);

struct Texture;

OBJMAP_DEF(Texture, texture,
  "SELECT tid, tname FROM assets.texture WHERE tid = ?1 OR tname = ?2",
  GLuint globj;
  const struct TextureTarget *target;
  GLsizei width0;
  GLsizei height0;
);

struct TextureLoaderArgs {
  int struct_version; /* must be zero */
  struct Texture *tex;
  GLuint tex_globj;
  const struct TextureTarget *target;
  const char *image_target_name;
  const struct TextureImageTarget *image_target;
  int64_t tex_imgid;
  int64_t load_order;
  GLint level;
  const char *loader_name;
  const char *loader_opts;
  size_t loader_opts_len;
  const uint8_t *data;
  size_t data_len;
  GLsizei *tex_width_out;
  GLsizei *tex_height_out;
};

typedef int (*texture_image_loader_func)(struct TextureLoaderArgs *);

void register_texture_image_loader_func(const char *name, texture_image_loader_func func, int struct_version);

struct Texture *load_texture_by_name(const char *name);
void bind_texture(GLenum unit, struct Texture *tex);

void register_texture_loader_webp();
void register_texture_loader_setminmagfilters();

void register_all_texture_loaders();

struct texture_binding {
  GLenum texunit;
  struct Texture *tex;
};

void texture_binding_make_active(struct texture_binding *binding);
void texture_bindings_make_active(struct texture_binding **bindings);

#endif
