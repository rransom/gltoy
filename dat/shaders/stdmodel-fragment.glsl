
precision mediump float;

uniform sampler2D mtl_tex_diffamb;
uniform sampler2D mtl_tex_normal;
uniform sampler2D mtl_tex_specular;

uniform vec4 mtl_color_ambient;
uniform vec4 mtl_color_diffuse;
uniform vec4 mtl_color_specular;

uniform float mtl_specular_exponent;

uniform vec3 light_dir_source;
uniform vec4 light_dir_color;
uniform vec4 light_ambient_color;

uniform vec3 eye_position;

varying vec3 geom_position;
varying vec3 geom_tangent_s, geom_tangent_t, geom_normal;
varying vec2 geom_texcoord;

vec3 compute_local_normal() {
  vec3 mapnormal = 2.0*texture2D(mtl_tex_normal, geom_texcoord).xyz - vec3(1.0, 1.0, 1.0);
  return normalize(mapnormal.x*geom_tangent_s + mapnormal.y*geom_tangent_t + mapnormal.z*geom_normal);
}

float specular_intensity(vec3 local_normal) {
  vec3 view_dir = geom_position - eye_position;
  vec3 blinn_phong_h = normalize(light_dir_source - view_dir);
  return pow(max(dot(local_normal, blinn_phong_h), 0.0), mtl_specular_exponent);
}

void main() {
  vec4 diffamb_tex = texture2D(mtl_tex_diffamb, geom_texcoord);
  vec4 ambient = mtl_color_ambient * diffamb_tex * light_ambient_color;
  vec3 local_normal = compute_local_normal();
  float diffuse_mag = max(dot(light_dir_source, local_normal), 0.0);
  vec4 diffuse = diffuse_mag * mtl_color_diffuse * light_dir_color * diffamb_tex;
  vec4 specular = specular_intensity(local_normal) * mtl_color_specular * texture2D(mtl_tex_specular, geom_texcoord);
  gl_FragColor = ambient + diffuse + specular;
}

