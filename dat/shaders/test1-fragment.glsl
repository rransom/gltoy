
varying mediump vec4 color;
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;

const mediump vec2 screen_size = vec2(800.0, 450.0);

void main() {
  //gl_FragColor = vec4(gl_FragCoord.x/800.0);
  //gl_FragColor = vec4(gl_FragCoord.y/450.0);
  //gl_FragColor = color;

  mediump vec2 texcoord = gl_FragCoord.xy/screen_size;

  mediump vec4 blend = color*vec4(0.5, 0.5, 2.0, 1.0);

  mediump vec4 color0 = texture2D(tex0, texcoord) * blend.r;
  mediump vec4 color1 = texture2D(tex1, texcoord) * blend.g;
  mediump vec4 color2 = texture2D(tex2, texcoord) * blend.b;

  gl_FragColor = color0 + color1 + color2;
  //gl_FragColor = (texture2D(tex1, texcoord)*2.0) + (color*0.1);

  //gl_FragColor = texture2D(tex2, vec2(0.5, 0.5)) + (color*0.1);

  gl_FragColor = texture2D(tex2, color.rg);
}

