
precision mediump float;

uniform sampler2D tex_diffuse;
uniform sampler2D tex_normal;
uniform sampler2D tex_specular;
uniform float specular_exponent;

uniform vec3 light_dir_source;
uniform vec4 light_dir_color;
uniform vec4 light_ambient_color;

uniform vec3 eye_position;

varying vec3 position;
varying vec3 tangent_s, tangent_t, normal;
varying vec2 texcoord;
varying vec4 color;

vec3 compute_local_normal() {
  vec3 mapnormal = 2.0*texture2D(tex_normal, texcoord).xyz - vec3(1.0, 1.0, 1.0);
  return normalize(mapnormal.x*tangent_s + mapnormal.y*tangent_t + mapnormal.z*normal);
}

float specular_intensity(vec3 local_normal) {
  vec3 view_dir = position - eye_position;
  vec3 blinn_phong_h = normalize(normalize(light_dir_source) - normalize(view_dir));
  return pow(max(dot(local_normal, blinn_phong_h), 0.0), specular_exponent);
}

void main() {
  vec4 diffusebase = color * texture2D(tex_diffuse, texcoord);
  vec4 ambient = diffusebase * light_ambient_color;
  vec3 local_normal = compute_local_normal();
  float diffusemag = max(dot(light_dir_source, local_normal), 0.0);
  vec4 diffuse = diffusemag * light_dir_color * diffusebase;
  vec4 specular = specular_intensity(local_normal) * texture2D(tex_specular, texcoord);
  gl_FragColor = ambient + diffuse + specular;
}

