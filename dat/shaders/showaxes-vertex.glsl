
attribute vec4 vPosition;
attribute vec4 vColor;

uniform mat4 transform;

varying vec4 color;

void main() {
  gl_Position = transform*vPosition;
  color = vColor;
}

