
attribute vec4 vPosition;
attribute vec2 vTexCoord;
attribute vec4 vColor;
attribute vec4 vTangentS;
attribute vec4 vTangentT;

uniform mat4 transform_pvm;
uniform mat3 transform_m;

varying vec3 position;
varying vec2 texcoord;
varying vec4 color;
varying vec3 tangent_s;
varying vec3 tangent_t;
varying vec3 normal;

void main() {
  gl_Position = transform_pvm*vPosition;
  position = transform_m*(vPosition.xyz / vPosition.w);
  tangent_s = transform_m*vTangentS.xyz;
  tangent_t = transform_m*vTangentT.xyz;
  normal = cross(tangent_s, tangent_t)*vTangentS.w*vTangentT.w;
  tangent_s /= dot(tangent_s, tangent_s);
  tangent_t /= dot(tangent_t, tangent_t);
  texcoord = vTexCoord;
  color = vColor;
}

