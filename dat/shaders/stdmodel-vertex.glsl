
attribute vec4 vPosition;
attribute vec2 vTexCoord;
attribute vec4 vNormal;
attribute vec4 vTangentS;
attribute vec4 vTangentT;

uniform mat4 transform_pvm;
uniform mat4 transform_m;
uniform mat3 transform_m_normal;

varying vec3 geom_position;
varying vec2 geom_texcoord;
varying vec3 geom_normal;
varying vec3 geom_tangent_s;
varying vec3 geom_tangent_t;

void main() {
  gl_Position = transform_pvm*vPosition;
  vec4 geom_position_proj = transform_m*vPosition;
  geom_position = (geom_position_proj.xyz / geom_position_proj.w);
  geom_tangent_s = (transform_m*vec4(vTangentS.xyz, 0.0)).xyz;
  geom_tangent_t = (transform_m*vec4(vTangentT.xyz, 0.0)).xyz;
  geom_normal = transform_m_normal*vNormal.xyz;
  geom_texcoord = vTexCoord;
}

