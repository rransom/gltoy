
varying mediump vec4 color;
varying mediump vec2 texcoord;

uniform sampler2D tex0;

void main() {
  gl_FragColor = color * texture2D(tex0, texcoord);
}

