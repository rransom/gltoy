
attribute vec4 vColor;     /* passed to frag shader */
attribute vec4 vTexRect;   /* x, y, w, h in pixels */
attribute vec4 vDispPos;   /* x, y, z, w of upper left corner */
attribute vec2 vRectPos;   /* x, y, ranging 0 to 1 */

uniform vec2 texture_scale;      /* 1/w, 1/h in pixels of texture */
uniform vec4 display_center;
uniform vec4 display_scale;      /* pixsize/2*w, -pixsize/h, 1/z_layer_scale, 1 for pixel art */

varying vec4 color;
varying vec2 texcoord;

void main() {
  vec4 dispPosOffset = vDispPos + vec4(vRectPos * vTexRect.zw, 0.0, 0.0) - display_center;
  vec4 dispPosScaled = dispPosOffset * display_scale;

  vec2 texCoord = vTexRect.xy + vRectPos * vTexRect.zw;

  gl_Position = dispPosScaled;
  color = vColor;
  texcoord = texCoord * texture_scale;
}

