
-- NOTE requires SQLite shell readfile function

BEGIN TRANSACTION;

CREATE TEMPORARY TABLE temp.simpleshaders(shpname, shname_fragment, shname_vertex);
INSERT INTO temp.simpleshaders(shpname, shname_fragment, shname_vertex)
  WITH simplershaders(shpname) AS (VALUES
         ('showaxes'),
         ('floor'),
         ('stdmodel'))
  SELECT shpname, shpname || '-fragment', shpname || '-vertex'
    FROM simplershaders;

INSERT INTO assets.datablob(bname, bdata)
  WITH simpleshaderbloblist(bname) AS
         (WITH simpleshaderlist(shname) AS
                 (SELECT shname_fragment FROM temp.simpleshaders
                    UNION
                  SELECT shname_vertex FROM temp.simpleshaders)
          SELECT 'shaders/' || shname || '.glsl' FROM simpleshaderlist)
  SELECT bname, readfile('dat/' || bname) FROM simpleshaderbloblist;

INSERT INTO assets.shader(shname, shtype, bid)
  WITH simpleshaderlist(shname, shtypename) AS
         (SELECT shname_fragment, 'fragment' FROM temp.simpleshaders
           UNION
          SELECT shname_vertex, 'vertex' FROM temp.simpleshaders)
  SELECT shname, shtype, bid FROM simpleshaderlist
      NATURAL JOIN assets.shadertype
      JOIN assets.datablob ON bname = 'shaders/' || shname || '.glsl';

INSERT INTO assets.shaderprogram(shpname)
  SELECT shpname FROM temp.simpleshaders;

INSERT INTO assets.shaderattachment(shpid, shid)
  WITH simpleshaderattlist(shpname, shname) AS
         (SELECT shpname, shname_fragment FROM temp.simpleshaders
           UNION
          SELECT shpname, shname_vertex FROM temp.simpleshaders)
  SELECT shpid, shid FROM simpleshaderattlist
      NATURAL JOIN assets.shader
      NATURAL JOIN assets.shaderprogram;

INSERT INTO assets.shaderattribbindingsetuse(shpid, shabsid)
  SELECT shpid, shabsid FROM temp.simpleshaders
      NATURAL JOIN assets.shaderprogram
      CROSS JOIN assets.shaderattribbindingset
    WHERE shabsname = 'standard-attrib-bindings';

DROP TABLE temp.simpleshaders;

COMMIT;

