
-- Generic blob storage

-- FIXME this should be a view and automatically uncompress bdata          
CREATE TABLE assets.datablob (
  bid INTEGER PRIMARY KEY,
  bname VARCHAR UNIQUE NOT NULL,
  bdata BLOB
);

-- GL shaders and programs

CREATE TABLE assets.shadertype (
  shtype INTEGER PRIMARY KEY,
  shtypename VARCHAR UNIQUE NOT NULL
);
INSERT INTO assets.shadertype(shtype, shtypename) VALUES
  (0x8B30, 'fragment'),
  (0x8B31, 'vertex'),
  (0x91B9, 'compute'),
  (0x8DD9, 'geometry'),
  (0x8E88, 'tess_control'),
  (0x8E87, 'tess_evaluation');

CREATE TABLE assets.shader (
  shid INTEGER PRIMARY KEY,
  shname VARCHAR UNIQUE NOT NULL,
  shtype INTEGER NOT NULL REFERENCES shadertype,
  bid INTEGER NOT NULL REFERENCES datablob
);
CREATE INDEX assets.shader_bid ON shader(bid);

CREATE TABLE assets.shaderprogram (
  shpid INTEGER PRIMARY KEY,
  shpname VARCHAR UNIQUE NOT NULL
);

CREATE TABLE assets.shaderattachment (
  shpid INTEGER NOT NULL REFERENCES shaderprogram,
  shid INTEGER NOT NULL REFERENCES shader,
  PRIMARY KEY(shpid, shid)
);
CREATE INDEX assets.shaderattachment_shid ON shaderattachment(shid);

CREATE TABLE assets.shaderattribbindingset (
  shabsid INTEGER PRIMARY KEY,
  shabsname VARCHAR UNIQUE NOT NULL
);

-- FIXME this should be a view loaded from a JSON blob in shaderattribbindingset
CREATE TABLE assets.shaderattribbinding (
  shabsid INTEGER NOT NULL REFERENCES shaderattribbindingset,
  shattname VARCHAR NOT NULL,
  shattindex INTEGER NOT NULL
);
CREATE INDEX assets.shaderattribbinding_shabsid ON shaderattribbinding(shabsid);

CREATE TABLE assets.shaderattribbindingsetuse (
  shpid INTEGER NOT NULL REFERENCES shaderprogram,
  shabsid INTEGER NOT NULL REFERENCES shaderattribbindingset,
  PRIMARY KEY(shpid, shabsid)
);
CREATE INDEX assets.shaderattribbindingsetuse_shabsid ON shaderattribbindingsetuse(shabsid);

-- GL textures

CREATE TABLE assets.texture (
  tid INTEGER PRIMARY KEY,
  tname VARCHAR UNIQUE NOT NULL,
  ttarget VARCHAR NOT NULL
);

CREATE TABLE assets.textureimage (
  timgid INTEGER PRIMARY KEY,
  tid INTEGER NOT NULL REFERENCES texture,
  timgloadorder INTEGER NOT NULL DEFAULT 0,
  timglevel INTEGER NOT NULL DEFAULT 0,
  timgtarget VARCHAR NOT NULL,
  timgloadername VARCHAR NOT NULL,
  timgloaderopts VARCHAR,
  bid INTEGER REFERENCES datablob
);
CREATE INDEX assets.textureimage_tid_timgloadorder on textureimage(tid, timgloadorder);
CREATE INDEX assets.textureimage_bid on textureimage(bid);

