
INSERT INTO assets.shaderattribbindingset(shabsname) VALUES
  ('standard-attrib-bindings');

INSERT INTO assets.shaderattribbinding(shabsid, shattname, shattindex)
  WITH mysetid(shabsid) AS
         (SELECT shabsid FROM assets.shaderattribbindingset WHERE shabsname = 'standard-attrib-bindings'),
       myattbind(n, i) AS (VALUES
         ('vPosition',  0),
         ('vColor',     1),
         ('vTexCoord',  2),
         ('vNormal',    3),
         ('vTangentS',  4),
         ('vTangentT',  5))
  SELECT shabsid, n, i FROM mysetid CROSS JOIN myattbind;

