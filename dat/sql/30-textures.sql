
-- NOTE requires SQLite shell readfile function

BEGIN TRANSACTION;

INSERT INTO assets.datablob(bname, bdata)
  WITH filenames(fn) AS (VALUES
         ('textures/town_tiles.webp'),
         ('textures/forest_tiles.webp'),
         ('textures/blowhard.webp'))
  SELECT fn, readfile('dat/' || fn) FROM filenames;

INSERT INTO assets.texture(tname, ttarget)
  WITH names(n) AS (VALUES
         ('town_tiles'),
         ('forest_tiles'),
         ('blowhard'))
  SELECT n, '2d' FROM names;

INSERT INTO assets.textureimage(tid, timgtarget, timgloadername, bid)
  WITH names(n) AS (VALUES
         ('town_tiles'),
         ('forest_tiles'),
         ('blowhard'))
  SELECT tid, '2d', 'webp', bid FROM names
      JOIN assets.texture ON tname = n
      JOIN assets.datablob ON bname = 'textures/' || n || '.webp';

INSERT INTO assets.textureimage(tid, timgloadorder, timgtarget, timgloadername, timgloaderopts)
  WITH names(n) AS (VALUES
         ('town_tiles'),
         ('forest_tiles'),
         ('blowhard'))
  SELECT tid, 1, '2d', 'setminmagfilters', 'nearest,nearest' FROM names
      JOIN assets.texture ON tname = n;

COMMIT;

