
.DELETE_ON_ERROR:

PKGS = sdl2 glesv2 sqlite3 libwebp

LEG = leg

CFLAGS = $(shell pkg-config --cflags $(PKGS) ) \
	-Isrc/ -Isrc/lib/ -Iout/ -Iout/lib/ \
	-g \

LIBS = $(shell pkg-config --libs $(PKGS) ) \
	-lm -lsbuf \
	-g \


OBJS = \
	out/main.o \
	out/db.o \
	out/lib/tree234.o \
	out/objmap.o \
	out/shader.o \
	out/texture.o \
	out/texture_webp.o \
	out/texture_setminmagfilters.o \
	out/texture_all_loaders.o \
	out/vecmat.o \
	out/asset.o \
	out/util.o \
	out/util_gl.o \
	out/scene_showaxes.o \
	out/scene_testblit1.o \
	out/input_keyboard.o \


HEADERS = \
	src/db.h \
	src/lib/tree234.h \
	src/objmap.h \
	src/shader.h \
	src/texture.h \
	src/vecmat.h \
	src/asset.h \
	src/util.h \
	src/scene.h \
	src/input.h \


out/gltoy: $(OBJS)
	$(CC) -o $@ $+ $(LIBS)

out/main.o: src/main.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/db.o: src/db.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/lib/tree234.o: src/lib/tree234.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/objmap.o: src/objmap.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/shader.o: src/shader.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/texture.o: src/texture.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/texture_emp.o: src/texture_emp.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/texture_pkm.o: src/texture_pkm.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/texture_webp.o: src/texture_webp.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/texture_setminmagfilters.o: src/texture_setminmagfilters.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/texture_all_loaders.o: src/texture_all_loaders.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/texture_any.o: src/texture_any.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/vecmat.o: src/vecmat.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/asset.o: src/asset.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/shader_asset.o: src/shader_asset.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/texture_asset.o: src/texture_asset.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/util.o: src/util.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/util_gl.o: src/util_gl.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/scene_showaxes.o: src/scene_showaxes.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/scene_testblit1.o: src/scene_testblit1.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/scene_floor.o: src/scene_floor.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/model.o: src/model.c $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

out/data_nature063.o: src/data_nature063.c out/data_nature063.inc $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<
out/data_nature063.inc: util/obj2c.py dat/models/orig/kenney/naturepack_extended/naturePack_063.obj
	python3 util/obj2c.py dat/models/orig/kenney/naturepack_extended/naturePack_063.obj nature063 >$@

out/input_keyboard.o: src/input_keyboard.c src/input_impl.h $(HEADERS)
	$(CC) -c -o $@ $(CFLAGS) $<

