
import collections
import io
import sys

logfile = sys.stderr
def log(*args):
    return print(*args, file=logfile)

class obj_model(object):
    __slots__ = ('v', 'vt', 'vn', 'faces', 'curmtl', 'mtllibs')
    def __init__(self):
        self.v = [None]
        self.vt = [None]
        self.vn = [None]
        self.faces = [None]
        self.curmtl = None
        self.mtllibs = [None]
        pass
    pass

obj_face_vert = collections.namedtuple('obj_face_vert', ('v', 'vt', 'vn'))
class obj_face(object):
    __slots__ = ('verts', 'mtl')
    pass

linehandlers = dict()

def parse_face_vert_index(istr, vlist):
    if istr == '':
        return None
    i = int(istr)
    if i > 0:
        return i
    elif i < 0:
        # FIXME? use a real exception here?               
        assert abs(i) <= len(vlist)
        return len(vlist) + 1 - i
    else:
        raise Exception('%r is not a valid index' % i)
    pass
def parse_face_vert(s, model):
    sparts = s.split('/')
    vlists = (model.v, model.vt, model.vn)
    return obj_face_vert(*(parse_face_vert_index(spart, vlist) for spart, vlist in zip(sparts, vlists)))
def parse_f(model, linetail):
    fverts = linetail.strip().split()
    face = obj_face()
    face.verts = tuple(parse_face_vert(fvert, model) for fvert in fverts)
    face.mtl = model.curmtl
    return model.faces.append(face)
linehandlers['f'] = parse_f

def parse_vertex_part(vlist, linetail):
    coords = linetail.strip().split()
    return vlist.append(tuple(coords))
def parse_v(model, linetail):
    return parse_vertex_part(model.v, linetail)
def parse_vt(model, linetail):
    return parse_vertex_part(model.vt, linetail)
def parse_vn(model, linetail):
    return parse_vertex_part(model.vn, linetail)
linehandlers['v'] = parse_v
linehandlers['vt'] = parse_vt
linehandlers['vn'] = parse_vn

def parse_mtllib(model, linetail):
    return model.mtllibs.append(linetail.strip())
linehandlers['mtllib'] = parse_mtllib

def parse_usemtl(model, linetail):
    model.curmtl = linetail.strip()
    pass
linehandlers['usemtl'] = parse_usemtl

def parse_line(model, line):
    line = line.lstrip()
    if line == '' or line[0] == '#':
        return
    linehead, linetail = line.split(None, 1)
    if linehead not in linehandlers:
        return log('ignoring line: %r' % line.rstrip())
    handler = linehandlers[linehead]
    return handler(model, linetail)

def parse_model_file(f):
    model = obj_model()
    for line in f:
        parse_line(model, line)
        pass
    return model


def check_model_vertex_materials(model):
    vmap = dict()
    for f in model.faces[1:]:
        for fvert in f.verts:
            if fvert.v in vmap:
                if vmap[fvert.v] != f.mtl:
                    log('vertex %r occurs with multiple materials (first %s, later %s)' %
                        (fvert.v, vmap[fvert.v], f.mtl))
                pass
            else:
                vmap[fvert.v] = f.mtl
                pass
            pass
        pass
    pass

def glify_model_vertex_arrays(model):
    rv = obj_model()
    rv.mtllibs = list(model.mtllibs)
    vertmap = dict()
    def glify_vertex(vertkey):
        if vertkey in vertmap:
            return vertmap[vertkey]
        fvert = vertkey[0]
        newidx = len(rv.v)
        rv.v.append(model.v[fvert.v])
        rv.vt.append(model.vt[fvert.vt] if fvert.vt is not None else ('0', '0'))
        rv.vn.append(model.vn[fvert.vn] if fvert.vn is not None else ('0', '0', '1'))
        vertmap[vertkey] = newidx
        return newidx
    for f in model.faces[1:]:
        newf = obj_face()
        newf.mtl = f.mtl
        newfverts = list()
        for fvert in f.verts:
            vertkey = (fvert,)
            newidx = glify_vertex(vertkey)
            newfverts.append(obj_face_vert(newidx, newidx, newidx))
            pass
        newf.verts = tuple(newfverts)
        rv.faces.append(newf)
        pass
    return rv


def check_c_identifier(s):
    assert s[0] in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_"
    for ch in s:
        assert ch in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_0123456789"
        pass
    pass

def print_c_model_consts(model, name, outfile=sys.stdout):
    check_c_identifier(name)
    assert len(model.v) == len(model.vt) and len(model.v) == len(model.vn)
    for v in model.v[1:]:
        assert isinstance(v, tuple) and len(v) == 3
        pass
    for vt in model.vt[1:]:
        assert isinstance(vt, tuple) and len(vt) == 2
        pass
    for vn in model.vn[1:]:
        assert isinstance(vn, tuple) and len(vn) == 3
        pass
    mtls = list()
    for f in model.faces[1:]:
        for fvert in f.verts:
            assert fvert.v == fvert.vt and fvert.v == fvert.vn
            pass
        if f.mtl not in mtls:
            mtls.append(f.mtl)
            pass
        pass
    outfile.write('static const GLfloat model_%s_position[%d] = {\n' % (name, len(model.v)*3))
    outfile.write('  0, 0, 0,\n')
    for i in range(1, len(model.v)):
        outfile.write('  %s, %s, %s,\n' % model.v[i])
        pass
    outfile.write('};\n')
    outfile.write('static const GLfloat model_%s_texcoord[%d] = {\n' % (name, len(model.v)*2))
    outfile.write('  0, 0,\n')
    for i in range(1, len(model.v)):
        outfile.write('  %s, %s,\n' % model.vt[i])
        pass
    outfile.write('};\n')
    outfile.write('static const GLfloat model_%s_normal[%d] = {\n' % (name, len(model.v)*3))
    outfile.write('  0, 0, 1,\n')
    for i in range(1, len(model.v)):
        outfile.write('  %s, %s, %s,\n' % model.vn[i])
        pass
    outfile.write('};\n')
    for mtl in mtls:
        check_c_identifier(mtl)
        outfile.write('static const GLushort model_%s_indices_%s[] = {\n' % (name, mtl))
        for f in model.faces[1:]:
            if f.mtl != mtl:
                continue
            outfile.write('  %d, %d, %d,\n' % tuple(fvert.v for fvert in f.verts))
            pass
        outfile.write('};\n')
        pass
    pass



def main(argv):
    infile_name = argv[1]
    model = None
    with open(infile_name, 'r') as infile:
        model = parse_model_file(infile)
        pass
    glified_model = glify_model_vertex_arrays(model)
    print_c_model_consts(glified_model, name=argv[2])
    pass

if __name__ == "__main__":
    import sys
    main(sys.argv)
    pass

